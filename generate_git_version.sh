#!/bin/sh
##
## To run automatically, add a step "Project Preferences" > C/C++ Build > Settings > Buid Steps
## Script will be run from Debug/ or Release/ directory
## 

git describe --abbrev=6 --dirty --always --tags | awk 'BEGIN{print "/***  AUTO-GENERATED FILE -- DO NOT EDIT.  ***/\n";} {sub(/^/, "#define GIT_VERSION \""); sub(/$/, "\""); print; }' > `pwd`/git_version.h
