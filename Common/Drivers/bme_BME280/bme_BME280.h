/******************************************************************************

Driver for BME280 pressure/temperature/humidity sensor.  Based on the
[Sparkfun BME280 Arduino library](https://github.com/sparkfun/SparkFun_BME280_Arduino_Library)
Original header provided below:


SparkFunBME280.h
BME280 Arduino and Teensy Driver
Marshall Taylor @ SparkFun Electronics
May 20, 2015
https://github.com/sparkfun/BME280_Breakout

Resources:
Uses Wire.h for i2c operation
Uses SPI.h for SPI operation

Development environment specifics:
Arduino IDE 1.6.4
Teensy loader 1.23

This code is released under the [MIT License](http://opensource.org/licenses/MIT).
Please review the LICENSE.md file included with this example. If you have any questions 
or concerns with licensing, please contact techsupport@sparkfun.com.
Distributed as-is; no warranty is given.

TODO:
	roll library ver to 2.0
	remove hard wire.
	write escalating examples


******************************************************************************/

// Test derived class for base class SparkFunIMU
#ifndef __INVADER_BME280_H__
#define __INVADER_BME280_H__

#include <stdbool.h>
#include <stdint.h>

#include "BME280_Registers.h"


bool bmeInit( void );

uint8_t bmeQueryChipID( void );

// Set the mode bits in the ctrl_meas register
//  Mode 00 = Sleep
//  01 and 10 = Forced
//  11 = Normal mode
void bmeSetMode( enum BmeMode mode);
uint8_t bmeGetMode();

// Set the temperature sample mode
void bmeSetTempOverSample(uint8_t overSampleAmount);

// Set the pressure sample mode
void bmeSetPressureOverSample(uint8_t overSampleAmount);

// Set the humidity sample mode
void bmeSetHumidityOverSample(uint8_t overSampleAmount);

//Set the standby bits in the config register
//tStandby can be:
//  0, 0.5ms
//  1, 62.5ms
//  2, 125ms
//  3, 250ms
//  4, 500ms
//  5, 1000ms
//  6, 10ms
//  7, 20ms
void bmeSetStandbyTime(uint8_t timeSetting);

// Set the filter
void bmeSetFilter(uint8_t filterSetting);

// Software reset routine
void bmeReset( void );


bool bmeIsMeasuring(void);


//Returns the values as floats.
float bmeReadPressure( void );
float bmeReadHumidity( void );

float bmeReadTempC( void );
float bmeReadTempF( void );

/// Returns measured pressure as an int.   Value in in Pascals
unsigned int bmeReadPressureUInt( void );

/// Returns measured humidity as an int.   Value is (humidity)*100  of relative humidity %
unsigned int bmeReadHumidityUInt( void );

/// Returns measured temperature as an int.   Value is (temperature C)*100
unsigned int bmeReadTempCUInt( void );

/// Returns measured temperature as an int.   Value is (temperature F)*100
unsigned int bmeReadTempFUInt( void );


//Class SensorSettings.  This object is used to hold settings data.  The application
//uses this classes' data directly.  The settings are adopted and sent to the sensor
//at special times, such as .begin.  Some are used for doing math.
//
//This is a kind of bloated way to do this.  The trade-off is that the user doesn't
//need to deal with #defines or enums with bizarre names.
//
//A power user would strip out SensorSettings entirely, and send specific read and
//write command directly to the IC. (ST #defines below)
//
//struct SensorSettings
//{
//  public:
//
//	//Main Interface and mode settings
//    uint8_t commInterface;
//    uint8_t I2CAddress;
//    uint8_t chipSelectPin;
//
//	//Deprecated settings
//	uint8_t runMode;
//	uint8_t tStandby;
//	uint8_t filter;
//	uint8_t tempOverSample;
//	uint8_t pressOverSample;
//	uint8_t humidOverSample;
//    float tempCorrection; // correction of temperature - added to the result
//};
//


//This is the main operational class of the driver.

//class BME280
//{
//  public:
//    //settings
//    SensorSettings settings;
//	SensorCalibration calibration;

//
//	//Constructor generates default SensorSettings.
//	//(over-ride after construction if desired)
//    BME280( void );
//    //~BME280() = default;
//
//	//Call to apply SensorSettings.
//	//This also gets the SensorCalibration constants
//    uint8_t begin( void );
//    bool beginSPI(uint8_t csPin); //Communicate using SPI
//    bool beginI2C(TwoWire &wirePort = Wire); //Called when user provides Wire port
//
//	#ifdef SoftwareWire_h
//	bool beginI2C(SoftwareWire &wirePort); //Called when user provides a softwareWire port
//	#endif
//
//	uint8_t getMode(void); //Get the current mode: sleep, forced, or normal
//	void setMode(uint8_t mode); //Set the current mode
//
//	void setTempOverSample(uint8_t overSampleAmount); //Set the temperature sample mode
//	void setPressureOverSample(uint8_t overSampleAmount); //Set the pressure sample mode
//	void setHumidityOverSample(uint8_t overSampleAmount); //Set the humidity sample mode
//	void setStandbyTime(uint8_t timeSetting); //Set the standby time between measurements
//	void setFilter(uint8_t filterSetting); //Set the filter
//
//	void setI2CAddress(uint8_t i2caddress); //Set the address the library should use to communicate. Use if address jumper is closed (0x76).
//
//	void setReferencePressure(float refPressure); //Allows user to set local sea level reference pressure
//	float getReferencePressure();
//
//	bool isMeasuring(void); //Returns true while the device is taking measurement
//
//	//Software reset routine
//	void reset( void );
//
//    //Returns the values as floats.
//    float readFloatPressure( void );
//	float readFloatAltitudeMeters( void );
//	float readFloatAltitudeFeet( void );
//
//	float readFloatHumidity( void );
//
//	//Temperature related methods
//    float readTempC( void );
//    float readTempF( void );
//
//	//Dewpoint related methods
//	//From Pavel-Sayekat: https://github.com/sparkfun/SparkFun_BME280_Breakout_Board/pull/6/files
//    double dewPointC(void);
//    double dewPointF(void);
//
//    //The following utilities read and write
//
//	//ReadRegisterRegion takes a uint8 array address as input and reads
//	//a chunk of memory into that array.
//    void readRegisterRegion(uint8_t*, uint8_t, uint8_t );
//	//readRegister reads one register
//    uint8_t readRegister(uint8_t);
//    //Reads two regs, LSByte then MSByte order, and concatenates them
//	//Used for two-byte reads
//	int16_t readRegisterInt16( uint8_t offset );
//	//Writes a byte;
//    void writeRegister(uint8_t, uint8_t);
//
//private:
//	uint8_t checkSampleValue(uint8_t userValue); //Checks for valid over sample values
//
//    uint8_t _wireType = HARD_WIRE; //Default to Wire.h
//    TwoWire *_hardPort = NO_WIRE; //The generic connection to user's chosen I2C hardware
//
//	#ifdef SoftwareWire_h
//	SoftwareWire *_softPort = NO_WIRE; //Or, the generic connection to software wire port
//	#endif
//
//	float _referencePressure = 101325.0; //Default but is changeable
//};

#endif  // End of __BME280_H__ definition check
