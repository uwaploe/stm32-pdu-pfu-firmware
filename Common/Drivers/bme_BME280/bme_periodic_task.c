

#include "bme_periodic_task.h"

#include "cmsis_os.h"

#include "BSP/invader_hw.h"

#include "Drivers/sic_io.h"
#include "Drivers/bme_BME280/bme_BME280.h"


 static void PeriodicTask_BmeSampling( void const *arg )
 {

	unsigned int temperature = 0.0, pressure = 0.0, humidity = 0.0;

	pressure = bmeReadPressureUInt();
	humidity = bmeReadHumidityUInt();
	temperature = bmeReadTempCUInt();

//	uint8_t chipID = bmeQueryChipID();

	SicNMEAPrintf("BMEPR", "%d,%d,%d", temperature, humidity, pressure );

 }


 osTimerDef(periodicBmeSampling, PeriodicTask_BmeSampling);
 osTimerId periodicBmeSamplingTimer;

 void Register_PeriodicTask_BmeSampling( int period )
 {
	 periodicBmeSamplingTimer = osTimerCreate(osTimer(periodicBmeSampling), osTimerPeriodic, (void *)NULL);
	 osTimerStart(periodicBmeSamplingTimer, period);
 }


