

#ifndef __INVADER_BME280_PERIODIC_TASK__
#define __INVADER_BME280_PERIODIC_TASK__


//=== BME Periodic tasks

#define DefaultPeriod_BmeSampling      1000

void Register_PeriodicTask_BmeSampling( int period );


#endif
