///
/// @file  periodic_task.c
/// #brief General (non-device-specific) periodic functions
///

#include <string.h>

#include "periodic_tasks.h"

#include "cmsis_os.h"

#include "BSP/invader_hw.h"
#include "BSP/hw_power_enable.h"

#include "Drivers/sic_io.h"
#include "Drivers/global_state.h"
#include "Drivers/focus_motor/focus_motor.h"

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup PeriodicTasks Periodic tasks @{


//=========================== Global State message ==================================

static void PeriodicTask_GlobalState( void const *arg )
{
	// \TODO.   Note this function will not run at exactly 1Hz, so this is approximate at best.
	// Could use a more accurate time source (RTC) if really required.
	GlobalState.uptime += (DefaultPeriod_GlobalState/1000);

	SicNMEAPrintf("STATS", "%d,%d", GlobalState.bootCount, GlobalState.uptime );

	if( errorCountersNonZero() ) {
		SicNMEAPrintf("ERROR", "%d", ErrorCounters.ioBufferUnavailable );
	}

	//=== Show current power status ===

	// As a temporary standin, create the power state message here rather
	// adding another timed task.
	SicBufferId_t id =  SicBufferReserve();

	SicBufferAppendStr( id, "$POWER," );

	for( unsigned char i = 0; i < numPowerEnables; i++ ) {
		if( strlen( powerEnables[i].name ) > 0 ) {
			SicBufferAppendPrintf(id, "%s,%d,", powerEnables[i].name, powerState(i) );
		}
	}

#ifdef INVADER_BOARD_PDU
	// Since the laser buses aren't named, do these manually for now
	SicBufferAppendPrintf(id, "LASER5,%d,", powerState(LASER_ISO_5) );
	SicBufferAppendPrintf(id, "LASER12,%d,", powerState(LASER_ISO_12) );
	SicBufferAppendPrintf(id, "LASER28,%d", powerState(LASER_ISO_28) );
#endif

	SicBufferFinalizeNMEA( id );
}

void Register_PeriodicTask_GlobalState( int period )
{
	  osTimerDef(periodicGlobalState, PeriodicTask_GlobalState );
	  osTimerId periodicGlobalStateTimer = osTimerCreate(osTimer(periodicGlobalState), osTimerPeriodic, (void *)NULL);
	  osTimerStart(periodicGlobalStateTimer, period);
}


//===================================================================


 static void PeriodicTask_PowerStatus( void const *arg )
 {

 	//=== Show current power status ===

 	// As a temporary standin, create the power state message here rather
 	// adding another timed task.
 	SicBufferId_t id =  SicBufferReserve();

 	SicBufferAppendStr( id, "$POWER," );

 	for( unsigned char i = 0; i < numPowerEnables; i++ ) {
 		if( strlen( powerEnables[i].name ) > 0 ) {
 			SicBufferAppendPrintf(id, "%s,%d,", powerEnables[i].name, powerState(i) );
 		}
 	}

 #ifdef INVADER_BOARD_PDU
 	// Since the laser buses aren't named, do these manually for now
 	SicBufferAppendPrintf(id, "LASER5,%d,", powerState(LASER_ISO_5) );
 	SicBufferAppendPrintf(id, "LASER12,%d,", powerState(LASER_ISO_12) );
 	SicBufferAppendPrintf(id, "LASER28,%d", powerState(LASER_ISO_28) );
 #endif

 	SicBufferFinalizeNMEA( id );
 }


  void Register_PeriodicTask_PowerStatus( int period )
  {
  	  osTimerDef(periodicPowerStatus, PeriodicTask_PowerStatus );
  	  osTimerId periodicPowerStatusTimer = osTimerCreate(osTimer(periodicPowerStatus), osTimerPeriodic, (void *)NULL);
  	  osTimerStart(periodicPowerStatusTimer, period);
  }


 /// @} @} @}


