///
///
///
///

#ifndef __INVADER_DAC_MCP4725_H__
#define __INVADER_DAC_MCP4725_H__

#include <stdbool.h>
#include <stdint.h>

enum MCP4725PowerDownMode {
	DAC_NORMAL = 0,  //!< DAC_NORMAL
	DAC_OFF_1k = 1,  //!< DAC_OFF_1k
	DAC_OFF_100K = 2,//!< DAC_OFF_100K
	DAC_OFF_500K = 3 //!< DAC_OFF_500K
};

struct MCP4725DACState {
	uint16_t dacOutput, eepromDacOutput;
	enum MCP4725PowerDownMode dacPowerDown, eepromPowerDown;
};

//=== These two functions must be provided somewhere else in the code
//void dacMCPWriteData(uint16_t devAddress, uint8_t *outData, uint16_t length);
//void dacMCPReadData(uint16_t devAddress, uint8_t *inData, uint16_t length);


bool dacMCPInit( uint8_t address );

bool dacMCPReadState( uint8_t address, struct MCP4725DACState *state );

/// Do a "fast write" and do not save to EEPROM
/// This function writes both output and mode
bool dacMCPFastWrite( uint8_t address, uint16_t out, enum MCP4725PowerDownMode pdMode );

/// A thin wrapper around dacMCPFastWrite which defaults to pdMode of DAC_NORMAL...
bool dacMCPWrite( uint8_t address, uint16_t out );

/// Full set which writes to EEPROM
bool dacMCPWriteAndSave( uint8_t address, uint16_t out, enum MCP4725PowerDownMode pdMode );

/// \brief Read current DAC output
uint16_t dacMCPRead( uint8_t address );


#endif
