///
///
///

#include <stdbool.h>
#include <stdint.h>

#include "dac_MCP4725.h"
#include "BSP/invader_hw.h"


// Store internal state for now
uint16_t _outValue;

bool dacMCPInit( uint8_t address ) {
	// No separate hardware configuration

	// At least confirm communications and do initial setup
	struct MCP4725DACState state;

	if( !dacMCPReadState( address, &state ) ) return false;

	SicPrintStr("!!   Focus motor DAC : Initialized\r\n");
	return true;
}


bool dacMCPReadState( uint8_t address, struct MCP4725DACState *state ) {

	uint8_t rxBuffer[5];

	dacMCPReadData( address, rxBuffer, 5 );

	state->dacOutput = rxBuffer[1]<<4;
	state->dacOutput |= (rxBuffer[2] & 0xF0)>>4;

	state->eepromDacOutput = (rxBuffer[3] & 0x0F)<<8;
	state->eepromDacOutput |= rxBuffer[4];

	state->dacPowerDown = (rxBuffer[0] & 0x06)>>1;
	state->eepromPowerDown = (rxBuffer[3] & 0x60)>>5;

	return true;
}

bool dacMCPFastWrite( uint8_t address, uint16_t out, enum MCP4725PowerDownMode pdMode ) {

	uint8_t txBuffer[2] = { (out & 0x0F00)>>8, (out & 0x00FF) };
	txBuffer[0] |= (pdMode << 12);

	dacMCPWriteData( address, txBuffer, 2 );

	return true;
}


// This does a "fast write"
bool dacMCPWrite( uint8_t address, uint16_t out ) {
	return dacMCPFastWrite( address, out, DAC_NORMAL );
}


bool dacMCPWriteAndSave( uint8_t address, uint16_t out, enum MCP4725PowerDownMode pdMode ) {
	uint8_t txBuffer[3];

	txBuffer[0] = 0x60 | (pdMode << 1);
	txBuffer[1] = (out & 0x0FF0) >> 4;
	txBuffer[2] = (out & 0x000F) << 4;

	dacMCPWriteData( address, txBuffer, 3 );

	return true;
}


uint16_t dacMCPRead( uint8_t address ) {
	struct MCP4725DACState state;
	if( !dacMCPReadState( address, &state ) ) return 0;

	return state.dacOutput;
}


