///
/// @file  global_state.h
/// #brief Global state variables
///

#ifndef __INVADER_GLOBAL_STATE_H__
#define __INVADER_GLOBAL_STATE_H__

#include <stdint.h>
#include "stm32f7xx_ll_rtc.h"

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup GlobalState Global state variables @{


#define BACKUP_REG_BOOT_COUNT (LL_RTC_BKP_DR10)

extern struct GlobalState_t {
	uint32_t  bootCount;
	uint64_t  uptime;
} GlobalState;

extern struct ErrorCounter_t {
	uint32_t  ioBufferUnavailable;
} ErrorCounters;

void initGlobalState();

bool errorCountersNonZero();

/// @} @} @}

#endif
