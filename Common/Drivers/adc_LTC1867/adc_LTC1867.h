/*************************************************************************

Interface to LTC1863 DAC on SPI Bus

Based on the Nathan Holmes' [Arduino LTC1863 Library](https://www.iascaled.com/store/ARD-LTC1863-LIB).
The original header copyright statement is included below.

Title:    ARD-LTC1863 / ARD-LTC1867 Driver Library
Authors:  Nathan D. Holmes <maverick@drgw.net>
File:     $Id: $
License:  GNU General Public License v3

LICENSE:
    Copyright (C) 2017 Nathan D. Holmes & Michael D. Petersen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

*************************************************************************/
#ifndef __INVADER_ADC_LTC1867_H__
#define __INVADER_ADC_LTC1867_H__

#include <stdbool.h>
#include <stdint.h>

#include "BSP/invader_hw.h"

// == The following functions define the Abstraction layer for this driver.
//    they must exist elsewhere in the binary
//
// The two extern types in adc_LTC1867_types must also exist in the application
bool adcSpiTransfer( uint8_t idx, uint8_t *dataOut, uint8_t *dataIn );
bool adcValidateIdx( uint8_t idx );

// ==  ==

#define ADC_BAD_INDEX -12345;

bool adcInitAll();
bool adcInit( enum LtcAdcs idx );

// == Functions for interfacing with a single ADC

// Only unipolar reads right now...
unsigned int adcRead( uint8_t idx );

unsigned int adcReadAndChangeChannel( uint8_t idx, uint8_t nextChannel);

void adcChangeChannel( uint8_t idx, uint8_t nextChannel);

void adcSleep( uint8_t idx );

void adcWake( uint8_t idx );


#endif


