/*************************************************************************

Interface to LTC1863 DAC on SPI Bus

Based on the Nathan Holmes' [Arduino LTC1863 Library](https://www.iascaled.com/store/ARD-LTC1863-LIB).
The original header copyright statement is included below.

Title:    ARD-LTC1863 / ARD-LTC1867 Driver Library
Authors:  Nathan D. Holmes <maverick@drgw.net>
File:     $Id: $
License:  GNU General Public License v3

LICENSE:
    Copyright (C) 2017 Nathan D. Holmes & Michael D. Petersen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

*************************************************************************/

#include <stdlib.h>
#include <stdint.h>

#include "adc_LTC1867.h"
#include "adc_LTC1867_registers.h"

//========================================================================

bool adcInitAll( ) {
	for( uint8_t i = 0; i < numLtcAdcs; i++ ) adcInit(i);

	return true;
}

bool adcInit( enum LtcAdcs idx ) {

	ltcAdcs[idx].currentConfig = LTC186X_CHAN_SINGLE_0P;

	// Push this initial config to the chip
	adcRead( idx );

	return true;
}

//========================================================================
//
//   Functions for interfacing with a given ADC
//
//========================================================================

static void internalChangeChannel(uint8_t idx, uint8_t nextChannel) {

	if( !adcValidateIdx( idx ) ) return;

	ltcAdcs[idx].currentConfig = _BV(LTC186X_CONFIG_UNI);

	// Bits are ordered strangely:
	//   Reorder [Bit 1, Bit 0, Odd] --> [Odd, Bit1, Bit 0]
	uint8_t chanBits = (((nextChannel & 0x01) << 2) | ((nextChannel & 0x06) >> 1)) << 4;
	chanBits |= 0x80; /// Set single-ended bit

	ltcAdcs[idx].currentConfig |= chanBits
		& (_BV(LTC186X_CONFIG_SINGLE_END)
			| _BV(LTC186X_CONFIG_ODD)
			| _BV(LTC186X_CONFIG_S1)
			| _BV(LTC186X_CONFIG_S0)
			| _BV(LTC186X_CONFIG_COM));
}

unsigned int adcRead( uint8_t idx ) {
	if( !adcValidateIdx( idx ) ) return 0;

	uint16_t retval = 0;

	// All interactions with the chip consist of a 16 bit read / write
	//   The write is 7 bits of configuration:
	//
	// followed by 8 bits of 0x00

	uint8_t txBuffer[2] = { ltcAdcs[idx].currentConfig, 0 },
			rxBuffer[2] = {0,0};

	adcSpiTransfer( idx, txBuffer, rxBuffer );

	/// For the LTC1867 data comes back as 16bit MSB
	retval = rxBuffer[0] << 8 | rxBuffer[1];

	return retval;
}

unsigned int adcReadAndChangeChannel( uint8_t idx, uint8_t nextChannel) {
	if( !adcValidateIdx( idx ) ) return ADC_BAD_INDEX;

	internalChangeChannel(idx, nextChannel);
	return adcRead(idx);
}

void adcChangeChannel( uint8_t idx, uint8_t nextChannel) {
	if( !adcValidateIdx( idx ) ) return;

	internalChangeChannel(idx, nextChannel );
	// Just discard the results
	adcRead(idx);
}

void adcSleep( uint8_t idx ) {
	if( !adcValidateIdx( idx ) ) return;

	ltcAdcs[idx].currentConfig |= _BV(LTC186X_CONFIG_SLP);

	// Just discard the results
	adcRead(idx);
}

void adcWake( uint8_t idx ) {
	if( !adcValidateIdx( idx ) ) return;

	uint8_t wasSleeping = ltcAdcs[idx].currentConfig & _BV(LTC186X_CONFIG_SLP);
	ltcAdcs[idx].currentConfig &= ~_BV(LTC186X_CONFIG_SLP);

	// Just discard the results
	adcRead(idx);

	// Wake-up time if we were really sleeping
	if (wasSleeping)
		HAL_Delay(1);
}


