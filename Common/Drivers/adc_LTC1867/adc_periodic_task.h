

#ifndef __INVADER_ADC_PERIODIC_TASK_H__
#define __INVADER_ADC_PERIODIC_TASK_H__


#include <stdint.h>
#include "cmsis_os.h"

#include "BSP/invader_hw.h"


#define DefaultPeriod_ADCSampling      1000


struct ADCPeriodicTask {
	enum LtcAdcs adc;
	char id;
	uint8_t channel;
	osTimerId timer;
	uint32_t values[8];
};

extern struct ADCPeriodicTask ADCTasks[ NUM_LTC_ADCS ];

void Register_PeriodicTask_SystemADCs( int period );
void Register_PeriodicTask_ADCSampling( enum LtcAdcs adc, int period );

void Halt_PeriodicTask_ADCSampling( enum LtcAdcs adc );


#endif
