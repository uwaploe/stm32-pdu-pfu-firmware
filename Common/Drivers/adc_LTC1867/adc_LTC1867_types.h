///
///
///

#ifndef __INVADER_ADC_LTC1867_TYPES_H__
#define __INVADER_ADC_LTC1867_TYPES_H__

#include <stdbool.h>
#include <stdint.h>

struct Ltc1867Entry {
	uint8_t adcIdx;
	uint8_t csIdx;
	uint8_t currentConfig;
};

// These two externs must be provided in the binary
extern struct Ltc1867Entry ltcAdcs[];
extern const int numLtcAdcs;


#endif
