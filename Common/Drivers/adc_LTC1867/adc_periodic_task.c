
#include <stdio.h>

#include "adc_periodic_task.h"

#include "cmsis_os.h"

#include "BSP/invader_hw.h"

#include "Drivers/sic_io.h"

#include "Drivers/adc_LTC1867/adc_LTC1867.h"

//=========================== ADCSampling ==================================


#ifdef INVADER_BOARD_PDU

struct ADCPeriodicTask ADCTasks[] = {
  { LTC_ADC0, '0', 0, 0 },
  { LTC_ADC1, '1', 0, 0 },
  { LTC_ADC2, '2', 0, 0 },
  { LTC_ADC3, '3', 0, 0 },
  { LTC_ADC4, '4', 0, 0 },
  { LTC_ADC5, '5', 0, 0 },
  { LTC_ADC_LASER, 'L', 0 },
};

#elif defined INVADER_BOARD_PFU

struct ADCPeriodicTask ADCTasks[] = {
  { LTC_ADC0, '0', 0 }
};

#endif




static void PeriodicTask_ADCSampling( void const *arg )
{
	/*
	// This is a little strange
	//Timer_t *timer = (Timer_t *)arg;
	//struct ADCPeriodicTask *adc = (struct ADCPeriodicTask *)timer->pvTimerID;
	struct ADCPeriodicTask *adc = (struct ADCPeriodicTask *)arg;

	if( adc->channel >= 7 ) {
		adc->values[adc->channel] = adcReadAndChangeChannel( adc->adc, 0 );
	} else {
		adc->values[adc->channel] = adcReadAndChangeChannel( adc->adc, adc->channel+1 );
	}
	//
	//HAL_Delay(1);

  	// Will hit this if immediately after sampling channel 7 and requesting 0
  	if( adc->channel == 7 ) {
  		char NMEATag[6];
  		snprintf( NMEATag, 6, "ADC%cR", adc->id );

  		SicNMEAPrintf(NMEATag, "%04X,%04X,%04X,%04X,%04X,%04X,%04X,%04X",
  				adc->values[0], adc->values[1], adc->values[2], adc->values[3],
				adc->values[4], adc->values[5], adc->values[6], adc->values[7] );
  	}

  	adc->channel++;
  	if( adc->channel >= 8 ) adc->channel = 0;
	 */


	struct ADCPeriodicTask *adc = (struct ADCPeriodicTask *)arg;

	//Change ADC to Channel 0
	adcChangeChannel( adc->adc, 0 );

	// Loop through all 8 ADC channels and save the conversion results
	for(int i=0;i<8;i++)
	{
		int nextChan = i+1;
		if(nextChan == 8)
			nextChan = 0;
		adc->values[i] = adcReadAndChangeChannel(adc->adc,nextChan);
	}

	//Assemble/Print the NMEA string
	char NMEATag[6];
	snprintf( NMEATag, 6, "ADC%cR", adc->id );

	SicNMEAPrintf(NMEATag, "%04X,%04X,%04X,%04X,%04X,%04X,%04X,%04X",
			adc->values[0], adc->values[1], adc->values[2], adc->values[3],
			adc->values[4], adc->values[5], adc->values[6], adc->values[7] );

	adc->channel = 0;
}


void Register_PeriodicTask_ADCSampling( enum LtcAdcs adc, int period )
{
	struct ADCPeriodicTask *task = &(ADCTasks[adc]);

	osTimerDef(periodicSamplingADC, PeriodicTask_ADCSampling);
	task->timer = osTimerCreate(osTimer(periodicSamplingADC), osTimerPeriodic, (void *)task);


	//\ todo Had case where we were running out of spots in the timer queue
	//       Need to check return from this function to assure timer is
	//       started properly.
	osTimerStart(task->timer, period);

}

void Register_PeriodicTask_SystemADCs( int period ) {
	Register_PeriodicTask_ADCSampling( LTC_ADC0, period );

#ifdef INVADER_BOARD_PDU
	Register_PeriodicTask_ADCSampling( LTC_ADC1, period );
	Register_PeriodicTask_ADCSampling( LTC_ADC2, period );
	Register_PeriodicTask_ADCSampling( LTC_ADC3, period );
	Register_PeriodicTask_ADCSampling( LTC_ADC4, period );
	Register_PeriodicTask_ADCSampling( LTC_ADC5, period );
#endif
}

void Halt_PeriodicTask_ADCSampling( enum LtcAdcs adc ) {
	struct ADCPeriodicTask *task = &(ADCTasks[adc]);

	// Can I guarantee this will be unset when initialized?
	if( task->timer ) osTimerStop( task->timer );
}

