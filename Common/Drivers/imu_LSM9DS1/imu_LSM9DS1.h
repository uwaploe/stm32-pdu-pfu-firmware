/******************************************************************************

Based on the [Sparkfun LSM9DS1 Library](https://github.com/sparkfun/SparkFun_LSM9DS1_Arduino_Library).
Original header comment follows:

SFE_LSM9DS1.h
SFE_LSM9DS1 Library Header File
Jim Lindblom @ SparkFun Electronics
Original Creation Date: February 27, 2015
https://github.com/sparkfun/LSM9DS1_Breakout

This file prototypes the LSM9DS1 class, implemented in SFE_LSM9DS1.cpp. In
addition, it defines every register in the LSM9DS1 (both the Gyro and Accel/
Magnetometer registers).

Development environment specifics:
	IDE: Arduino 1.6.0
	Hardware Platform: Arduino Uno
	LSM9DS1 Breakout Version: 1.0

This code is beerware; if you see me (or any other SparkFun employee) at the
local, and you've found our code helpful, please buy us a round!

Distributed as-is; no warranty is given.
******************************************************************************/
#ifndef __INVADER_IMU_LSM9DS1_H__
#define __INVADER_IMU_LSM9DS1_H__

#include "stdint.h"

#include "LSM9DS1_Registers.h"
#include "LSM9DS1_Types.h"


bool imuInit();

uint8_t imuAccelAvailable();
uint8_t imuGyroAvailable();
uint8_t imuTempAvailable();
uint8_t imuMagAvailable(enum lsm9ds1_axis axis);

void imuReadAccelRaw( struct RawValues *val );
int16_t imuReadAccelAxisRaw( enum lsm9ds1_axis axis);
void imuScaleAccel( struct RawValues *in, struct ScaledValues *out );
void imuReadAccel( struct ScaledValues *val );

void imuReadMagRaw( struct RawValues *val );
int16_t imuReadMagAxisRaw( enum lsm9ds1_axis axis);
void imuScaleMag( struct RawValues *in, struct ScaledValues *out );
void imuReadMag( struct ScaledValues *val );

void imuReadGyroRaw( struct RawValues *val );
int16_t imuReadGyroAxisRaw( enum lsm9ds1_axis axis);
void imuScaleGyro( struct RawValues *in, struct ScaledValues *out );
void imuReadGyro( struct ScaledValues *val );

int16_t imuReadTemp();

//=== Hardware interface functions, must be provided ===

uint8_t imuXgReadByte( uint8_t addr );
uint8_t imuXgReadBytes( uint8_t addr, uint8_t *data, uint16_t length );
void    imuXgWriteByte( uint8_t addr, uint8_t data );

uint8_t imuMReadByte( uint8_t addr );
uint8_t imuMReadBytes( uint8_t addr, uint8_t *data, uint16_t length );
void    imuMWriteByte( uint8_t addr, uint8_t data );


#endif // SFE_LSM9DS1_H //
