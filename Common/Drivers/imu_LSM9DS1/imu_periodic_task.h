

#ifndef __INVADER_IMU_PERIODIC_TASK_H__
#define __INVADER_IMU_PERIODIC_TASK_H__


//=== IMU sampling tasks

#define DefaultPeriod_ImuSampling      1000
void Register_PeriodicTask_ImuSampling( int period );


#endif
