///
/// @file  imu_periodic_task.c
/// #brief Periodic task for displaying IMU data
///


#include <math.h>
#include <stdint.h>

#include "imu_periodic_task.h"

#include "cmsis_os.h"

#include "BSP/invader_hw.h"

#include "Drivers/sic_io.h"

#include "Drivers/imu_LSM9DS1/imu_LSM9DS1.h"
#include "Drivers/imu_LSM9DS1/LSM9DS1_Types.h"


static  void PeriodicTask_ImuSampling( void const *arg )
{
	struct RawValues rawGyro, rawAccel, rawMag;
	imuInitRaw( &rawGyro );
	imuInitRaw( &rawAccel );
	imuInitRaw( &rawMag );

	imuReadAccelRaw( &rawAccel );
	imuReadGyroRaw( &rawGyro );
	imuReadMagRaw( &rawMag );
	uint16_t temperature = imuReadTemp();

	SicNMEAPrintf("IMAGR", "%d,%d,%d,%d,%d,%d,%d", rawAccel.x, rawAccel.y, rawAccel.z, rawGyro.x,  rawGyro.y,  rawGyro.z, temperature );
	SicNMEAPrintf("IMMGR", "%d,%d,%d", rawMag.x, rawMag.y, rawMag.z );

	// Don't read the IMU again, instead scale
	struct ScaledValues scaledGyro, scaledAccel, scaledMag;
	imuInitScaled( &scaledGyro );
	imuInitScaled( &scaledAccel );
	imuInitScaled( &scaledMag );

	imuScaleAccel( &rawAccel, &scaledAccel );
	imuScaleGyro( &rawGyro, &scaledGyro );
	imuScaleMag( &rawMag, &scaledMag );

//	imuReadAccel( &scaledAccel );
//	imuReadGyro( &scaledGyro );
//
//	SicNMEAPrintf("IMAGS", "%.5f,%.5f,%.5f,%.5f,%.5f,%.5f,%d", scaledAccel.x, scaledAccel.y, scaledAccel.z, scaledGyro.x,  scaledGyro.y,  scaledGyro.z, temperature );

	const int AccelScalar = 1000, GyroScalar = 1000, MagScalar=1000, TempScalar = 10;
	SicNMEAPrintf("IMAGS", "%d,%d,%d,%d,%d,%d,%d",
					(int)floor(scaledAccel.x * AccelScalar), (int)floor(scaledAccel.y * AccelScalar), (int)floor(scaledAccel.z * AccelScalar),
					(int)floor(scaledGyro.x  * GyroScalar),  (int)floor(scaledGyro.y * GyroScalar),   (int)floor(scaledGyro.z * GyroScalar),
					(int)floor(temperature * TempScalar) );
	SicNMEAPrintf("IMMGS", "%d,%d,%d",
					(int)floor(scaledMag.x * MagScalar), (int)floor(scaledMag.y * MagScalar), (int)floor(scaledMag.z * MagScalar));


}

osTimerDef(periodicImuSampling, PeriodicTask_ImuSampling);
osTimerId periodicImuSamplingTimer;


void Register_PeriodicTask_ImuSampling( int period )
{
	periodicImuSamplingTimer = osTimerCreate(osTimer(periodicImuSampling), osTimerPeriodic, (void *)NULL);
	osTimerStart(periodicImuSamplingTimer, period);
}




