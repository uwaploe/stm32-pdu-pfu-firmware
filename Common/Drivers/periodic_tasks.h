///
/// @file  periodic_task.h
/// #brief General (non-device-specific) periodic functions
///

#ifndef __INVADER_PERIODIC_TASKS_H__
#define __INVADER_PERIODIC_TASKS_H__

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup PeriodicTasks Periodic tasks @{


//=== Global state message
#define DefaultPeriod_GlobalState      5000
void Register_PeriodicTask_GlobalState( int period );


//=== Power status message
#define DefaultPeriod_PowerStatus      5000
void Register_PeriodicTask_PoewrStatus( int period );

/// @} @} @}

#endif
