/*

 Originally based on [this](https://github.com/hallard/DAC7565) Arduino library for the
 DAC8562.  The original file header follows:

  DAC8562.h
  
  Arduino library for Texas Instruments DAC8562 2-channel 16-bit SPI DAC
  
  External voltage reference; full-scale is 2*vref
  
  To use:
    pleae see the examples provided in this library
  
  Wiring:
    Use any digital pin for !CS (chip select).
    Data and clock use hardware SPI connections.
      For "classic" Arduinos (Uno, Duemilanove, etc.), data = pin 11, clock = pin 13
      For Teensy 2.0, data = B2 (#2), clock = B1 (#1)
      For Teensy 3.0, data = 11 (DOUT), clock = 13 (SCK)

  inspired by @machinesalem
    
  2018-07-24 @km7,  (cc) https://creativecommons.org/licenses/by/3.0/
*/

#ifndef __INVADER_DAC8562_H__
#define __INVADER_DAC8562_H__

#include <stdbool.h>
#include <stdint.h>

/// Initialize the DAC8562 (only one in the system)
bool dac8562Init( void );

/// Set the output voltage from the DAC output A
///
/// Right now _only output A is available_
void dac8562SetVoltage(float output);
void dac8562Set(uint16_t output);

/// \brief Returns the current DAC value (cached in driver, not read from hardware)
uint16_t dac8562Get( void );


//=== This interface function must be provided elsewhere ==
bool dac8562SpiTransfer( uint8_t cmd, uint16_t data );


#endif
