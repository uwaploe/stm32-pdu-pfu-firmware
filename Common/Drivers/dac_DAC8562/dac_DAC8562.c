/*
 Originally based on [this](https://github.com/hallard/DAC7565) Arduino library for the
 DAC8562.  The original file header follows:

  DAC8562.cpp
  
  Arduino library for Texas Instruments DAC8562 2-channel 16-bit SPI DAC
  
  2018-07-24 @km7,  (cc) https://creativecommons.org/licenses/by/3.0/
*/

#include "dac_DAC8562.h"
#include "dac_DAC8562_registers.h"


float _vref = 5.0;  // Using internval Vref=2.5 with gain of 2

// For now, just cache the latest value
uint16_t _channelA;

bool dac8562Init( void ) {

  dac8562SpiTransfer(CMD_RESET_ALL_REG, DATA_RESET_ALL_REG);      // reset
  dac8562SpiTransfer(CMD_SETA_UPDATEA,	0);
  dac8562SpiTransfer(CMD_PWR_UP_A_B,    DATA_PWR_UP_A_ONLY);        // power up A only
  dac8562SpiTransfer(CMD_PWR_UP_A_B,    DATA_PWR_B_HIZ);            // set B to hi-z
  //dac8562SpiTransfer(CMD_PWR_UP_A_B,    DATA_PWR_UP_A_B);			// Enable both outputs A and B

  dac8562SpiTransfer(CMD_INTERNAL_REF_EN, DATA_INTERNAL_REF_EN);      // enable internal reference
  dac8562SpiTransfer(CMD_GAIN,          DATA_GAIN_B2_A2);            // set multiplier
  dac8562SpiTransfer(CMD_LDAC_DIS,      DATA_LDAC_DIS);          // update the caches

  _channelA = 0;

  return false;
}


static uint16_t dacVoltageConvert(float voltage)
{
  uint16_t D;

  // Uh, what kind of witchcraft is this.
  //voltage = voltage/_vref * 5;

  voltage = voltage / 6  + 2.5;   //based on the manual provided by texas instruments

  D = (uint16_t)(65536 * voltage / 5);

  if(D < 32768)
  {
    D -= 100;     //fix the errors
  }

  return D;
};

/// \brief Update DAC output for channel A
static void dacWriteA(uint16_t output) {
  _channelA = output;
  dac8562SpiTransfer(CMD_SETA_UPDATEA,output);
}


/// \brief Update DAC output for channel B
static void dacWriteB(uint16_t output) {
  dac8562SpiTransfer(CMD_SETB_UPDATEB,output);
}


/// \brief Set DAC8562 output from float voltage
void dac8562SetVoltage(float output) {
  dacWriteA(dacVoltageConvert(output));
}

void dac8562Set(uint16_t output) {
	dacWriteA( output );
//	dacWriteB( output );
}



uint16_t dac8562Get() {
	return _channelA;
}



