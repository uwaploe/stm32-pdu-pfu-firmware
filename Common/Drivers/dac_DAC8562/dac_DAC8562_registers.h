

#ifndef __INVADER_DAC8562_REGISTERS_H__
#define __INVADER_DAC8562_REGISTERS_H__


// Every SPI transfer to the DAC8562 consists of 24 bits:  a command bytes and 16bits of data
//
// The command bytes consists of (from MSB):
//    - two "don't care" bits
//    - three command bits
//    - three address bits

// Commands bits:
//
//    0 0 0   Write to input register n
//    0 0 1   Software LDAC, updated DAC register n
//    0 1 0   Write to input register n and update all DAC registers
//    0 1 1   Write to input register n and update DAC register n
//    1 0 0   Set DAC power up or down mode
//    1 0 1   Software reset
//    1 1 0   Set LDAC registers
//    1 1 1   Enable or disable internal reference

// Address bits
//    0 0 0   DAC-A
//    0 0 1   DAC-B
//    0 1 0   Gain (only with command 000)
//    0 1 1 through 1 1 0 reserved
//    1 1 1   DAC-A and DAC-B

#define CMD_SETA_UPDATEA          0x18  // Write to register A and update output A
#define CMD_SETB_UPDATEB          0x19  // Write to register B and update output B
#define CMD_UPDATE_ALL_DACS       0x0F  // 更新两路寄存器命令，后16位只需时钟即可

#define CMD_GAIN                  0x02  // 内部放大倍数命令
#define DATA_GAIN_B2_A2           0x0000  // B路2倍，A路1倍
#define DATA_GAIN_B2_A1           0x0001  // B路1倍，A路2倍
#define DATA_GAIN_B1_A2           0x0002  // B路2倍，A路2倍
#define DATA_GAIN_B1_A1           0x0003  // B路1倍，A路1倍

#define CMD_PWR_UP_A_B            0x20    // Command for all power up/down commands
#define DATA_PWR_UP_A_B           0x0003  // 数据：Power up DAC-A and DAC-B  data
#define DATA_PWR_UP_A_ONLY        0x0001  // Power up DAC-A only

#define DATA_PWR_B_HIZ            0x0032  // 0x0030 == Hi-Z;  0x0002 = DAC-B only


#define CMD_RESET_ALL_REG         0x28  // 命令：所有寄存器复位、清空寄存器
#define DATA_RESET_ALL_REG        0x0001  // 数据：所有寄存器复位、清空寄存器

#define CMD_LDAC_DIS              0x30  // LDAC脚功能命令
#define DATA_LDAC_DIS             0x0003  // LDAC脚不起作用

#define CMD_INTERNAL_REF_DIS      0x38  // 命令：Disable internal reference and reset DACs to gain = 1
#define DATA_INTERNAL_REF_DIS     0x0000  // 数据：Disable internal reference and reset DACs to gain = 1

#define CMD_INTERNAL_REF_EN       0x38  // 命令：Enable Internal Reference & reset DACs to gain = 2
#define DATA_INTERNAL_REF_EN      0x0001  // 数据：Enable Internal Reference & reset DACs to gain = 2

#endif
