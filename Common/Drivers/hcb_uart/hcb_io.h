///
/// hcb_uart.h
///

#ifndef __INVADER_DRIVER_HCB_IO_H__
#define __INVADER_DRIVER_HCB_IO_H__

#include <stdint.h>
#include <cmsis_os.h>

#include "stm32f7xx_hal.h"

/// \addtogroup Common
/// @{
///
/// \addtogroup Drivers
/// @{
///
/// \addtogroup HcbIO Heater Control Board Serial Comms
/// @{

//#define HCB_NOT_TRANSMITTING 255

#define HCB_BUFFER_OK 		0
#define HCB_BUFFER_BUSY     -1
#define HCB_BUFFER_OVERRUN  -125
#define HCB_BUFFER_ERROR 	-127

#define HCB_NUM_RX_BUFFERS 2
#define HCB_BUFFER_LENGTH 256
#define HCB_BUFFER_MSG_MAX ( HCB_BUFFER_LENGTH - 20 )    // Won't overflow HCB IO buffers once the $<nmea tag> and checksum are added

osMessageQId  HcbMsgBox;

typedef struct HcbTxBuffer {
	char buf[HCB_BUFFER_LENGTH];
	int  length;
	bool transmitting;
} HcbTxBuffer_t;

typedef struct HcbRxBuffer {
	uint8_t buf[ HCB_BUFFER_LENGTH ];
	unsigned int length;
} HcbRxBuffer_t;

extern HcbTxBuffer_t _HcbTxBuffer;
extern HcbRxBuffer_t _HcbRxBuffers[HCB_NUM_RX_BUFFERS];
extern uint8_t       _HcbActiveRxBuffer;

void ProcessHcbInputThread(void const * argument);
extern const osThreadDef_t os_thread_def_ProcessHcbInput;

void HcbBuffersInitialize( void );

// Present a very simplified interface to the HCB
int HcbPrint( const uint8_t *data, uint8_t len );
int HcbPrintStr( const char *str );
int HcbPrintf( const char *fmtstr, ... );

void HcbTxBufferPublish();
void HcbTxBufferTransmitted();

//void HCBTxBufferRelease(void);
//void HCBTxBufferPublish(void);
//void HCBTxBufferFinalize(void);
//int HCBTxBufferAppend(const uint8_t *data, int len );
//int HCBTxBufferAppendStr(const char *s );
//void HCBBufferReserve( void );

/// @} @} @}

#endif
