
#include "BSP/invader_hw.h"

#include <stdint.h>
#include <stdbool.h>

#include "stm32f7xx_hal.h"
#include "hcb_uart.h"

#include "Drivers/hcb_uart/hcb_uart.h"
#include "Drivers/hcb_uart/hcb_io.h"

UART_HandleTypeDef HCBUartHandle;
uint8_t _rxTempBuffer[2];


void HcbTransmitBuffer( void ) {
	if( HCBUartHandle.gState == HAL_UART_STATE_READY )
	{
		HAL_StatusTypeDef retval = HAL_UART_Transmit_IT( &HCBUartHandle, (uint8_t *)_HcbTxBuffer.buf, _HcbTxBuffer.length );
	}
}

///
/// Called by HAL when the transmission has completed
///
void HcbTxComplete(UART_HandleTypeDef *huart)
{
	HcbTxBufferTransmitted();
}


// TODO is this fast enough or do I need to make it ISR safe?
void HcbRxComplete(UART_HandleTypeDef *huart) {
	// Process incoming message

	const uint8_t inchar = _rxTempBuffer[0];
	const uint8_t activeRxBuffer = _HcbActiveRxBuffer;

	if( inchar == '\r' || inchar == '\n' ) {
		if( _HcbRxBuffers[activeRxBuffer].length > 0 ) {
			osMessagePut( HcbMsgBox, activeRxBuffer, 0 );
		}
	} else {

		_HcbRxBuffers[activeRxBuffer].buf[ _HcbRxBuffers[activeRxBuffer].length++ ] = _rxTempBuffer[0];

		if( _HcbRxBuffers[activeRxBuffer].length >= HCB_BUFFER_MSG_MAX ) {
			osMessagePut( HcbMsgBox, activeRxBuffer, 0 );
		}
	}

	HAL_UART_Receive_IT(&HCBUartHandle, _rxTempBuffer, 1 );

}



//========================================================================
//
//
//========================================================================


void HcbHALInit( void )
{
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  HCB_USARTx_GPIO_CLK_ENABLE();
  HCB_USARTx_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* UART TX GPIO pin configuration  */
  GPIO_InitTypeDef  GPIO_InitStruct;

  GPIO_InitStruct.Pin       = HCB_USARTx_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = HCB_USARTx_AF;

  HAL_GPIO_Init(HCB_USARTx_GPIO_PORT, &GPIO_InitStruct);

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = HCB_USARTx_RX_PIN;

  HAL_GPIO_Init(HCB_USARTx_GPIO_PORT, &GPIO_InitStruct);

  /*##-3- Configure the NVIC for UART ########################################*/
  /* NVIC for USARTx */
  HAL_NVIC_SetPriority(HCB_USARTx_IRQn, HCB_NVIC_PRIORITY, 1 );
  HAL_NVIC_EnableIRQ(HCB_USARTx_IRQn);
}

void HcbHALDeInit(void)
{
	HCB_USARTx_FORCE_RESET();
	HCB_USARTx_RELEASE_RESET();

	HAL_GPIO_DeInit(HCB_USARTx_GPIO_PORT, HCB_USARTx_TX_PIN);
	HAL_GPIO_DeInit(HCB_USARTx_GPIO_PORT, HCB_USARTx_RX_PIN);

	HAL_NVIC_DisableIRQ(HCB_USARTx_IRQn);
}


/**
  * @brief  Function called in case of error detected in USART IT Handler
  *
  * \todo  Make this do something sensical
  *
  * @param  None
  * @retval None
  */
static void HCB_Error_Callback(void)
{
  __IO uint32_t isr_reg;

  /* Disable USARTx_IRQn */
  NVIC_DisableIRQ(HCB_USARTx_IRQn);

  /* Error handling example :
    - Read USART ISR register to identify flag that leads to IT raising
    - Perform corresponding error handling treatment according to flag
  */
//  isr_reg = LL_USART_ReadReg(SIC_USARTx, ISR);

//  if (isr_reg & LL_USART_ISR_NE)
//  {
//    /* case Noise Error flag is raised : ... */
//    //LED_Blinking(LED_BLINK_FAST);
//  }
//  else
//  {
//    /* Unexpected IT source : Set LED to Blinking mode to indicate error occurs */
//    //LED_Blinking(LED_BLINK_ERROR);
//  }
}

/**
  * @brief  This function configures USARTx Instance.
  * @note   This function is used to :
  *         -1- Enable GPIO clock and configures the USART pins.
  *         -2- NVIC Configuration for USART interrupts.
  *         -3- Enable the USART peripheral clock and clock source.
  *         -4- Configure USART functional parameters.
  *         -5- Enable USART.
  * @note   Peripheral configuration is minimal configuration from reset values.
  *         Thus, some useless LL unitary functions calls below are provided as
  *         commented examples - setting is default configuration from reset.
  * @param  None
  * @retval None
  */
void HcbUartInit(void)
{
	HCBUartHandle.Instance        = HCB_USARTx;

	HCBUartHandle.Init.BaudRate   = HCB_USARTx_BAUD;
	HCBUartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	HCBUartHandle.Init.StopBits   = UART_STOPBITS_1;
	HCBUartHandle.Init.Parity     = UART_PARITY_NONE;
	HCBUartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	HCBUartHandle.Init.Mode       = UART_MODE_TX_RX;

	if(HAL_UART_Init(&HCBUartHandle) != HAL_OK)
	{
	  /* Initialization Error */
		HCB_Error_Callback();
	}

	HcbBuffersInitialize();

	// Kick off receive cycle
	HAL_UART_Receive_IT(&HCBUartHandle, _rxTempBuffer, 1 );

}

