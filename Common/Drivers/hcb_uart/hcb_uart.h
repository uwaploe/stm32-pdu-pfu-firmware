///
/// hcb_uart.h
///

#ifndef __INVADER_DRIVER_HCB_UART_H__
#define __INVADER_DRIVER_HCB_UART_H__

#include <stdint.h>
#include <cmsis_os.h>

#include "stm32f7xx_hal.h"

/// \addtogroup Common
/// @{
///
/// \addtogroup Drivers
/// @{
///
/// \addtogroup HcbIO Heater Control Board Serial Comms
/// @{

extern UART_HandleTypeDef HCBUartHandle;

void HcbHALInit(void);
void HcbHALDeInit(void);

void HcbUartInit(void);

void HcbTransmitBuffer();

void HcbTxComplete(UART_HandleTypeDef *huart);
void HcbRxComplete(UART_HandleTypeDef *huart);

/// @} @} @}

#endif
