
#include "BSP/invader_hw.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>

#include "hcb_io.h"
#include "hcb_uart.h"

#include "Drivers/sic_io.h"

HcbTxBuffer_t _HcbTxBuffer;

// -- Implement a simple double buffer on rx from the HCB
HcbRxBuffer_t _HcbRxBuffers[HCB_NUM_RX_BUFFERS];
uint8_t _HcbActiveRxBuffer;

// Declare RTOS message queue for communications
osMessageQDef(HcbMsgBox, 2, uint8_t );
osMessageQId  HcbMsgBox;

void rotateActiveRxBuffer()
{
	_HcbActiveRxBuffer++;
	if(_HcbActiveRxBuffer >= HCB_NUM_RX_BUFFERS ) _HcbActiveRxBuffer = 0;
}

void ProcessHcbInputThread(void const * argument)
{

	while(1) {

		osEvent event = osMessageGet( HcbMsgBox, osWaitForever );

		if( event.status == osEventMessage ) {
			uint8_t val = event.value.v;

			uint8_t activeBuffer = _HcbActiveRxBuffer;
			rotateActiveRxBuffer();

			// Quickly grab a copy of incoming message
			//  -- assumes this happens before the next message comes in
			HcbRxBuffer_t msg;
			msg.length = _HcbRxBuffers[activeBuffer].length;
			memcpy( msg.buf, _HcbRxBuffers[activeBuffer].buf, msg.length );

			// Terminate message
			msg.buf[ msg.length ] = '\0';

			///=== For now, just pass message on to SIC
			if( msg.length > 0 ) {
				SicNMEAPrint("HCBRX", msg.buf, msg.length);
			}

			/// Additional HCB message parsing could happen here....
		}

  }

	osThreadTerminate(NULL);
}

osThreadDef(ProcessHcbInput, ProcessHcbInputThread, osPriorityLow, 0, configMINIMAL_STACK_SIZE * 2);

//===

void HcbBuffersInitialize( void ) {

	_HcbTxBuffer.length = 0;
	_HcbTxBuffer.transmitting = false;

	for( uint8_t i = 0; i < HCB_NUM_RX_BUFFERS; ++i ) {
		_HcbRxBuffers[i].length = 0;
	}

  _HcbActiveRxBuffer = 0;
  InputMsgBox = osMessageCreate(osMessageQ(HcbMsgBox), NULL);  // create msg queue

}


//==== Functions for handling Tx Buffer assembly and transmission ====

void HcbTxBufferPublish()
{
	if( _HcbTxBuffer.length == 0 ) return;
	_HcbTxBuffer.transmitting = true;

	HcbTransmitBuffer();
}

void HcbTxBufferTransmitted()
{
	_HcbTxBuffer.length = 0;
	_HcbTxBuffer.transmitting = false;
}



///=== Internal functions

static int HcbTxBufferAppend( const uint8_t *data, uint8_t len )
{
	if( _HcbTxBuffer.transmitting ) return HCB_BUFFER_BUSY;
	if( (len + _HcbTxBuffer.length) > HCB_BUFFER_LENGTH ) return HCB_BUFFER_OVERRUN;

	memcpy( _HcbTxBuffer.buf[ _HcbTxBuffer.length ], data, len );
	_HcbTxBuffer.length += len;

	return _HcbTxBuffer.length;
}

static int HcbTxBufferAppendStr( const char *s )
{
	if( _HcbTxBuffer.transmitting ) return HCB_BUFFER_BUSY;

	size_t i = 0;
	while( s[i] && i < HCB_BUFFER_LENGTH ) { i += 1; }

	if( i >= HCB_BUFFER_LENGTH ) return HCB_BUFFER_OVERRUN;

	return HcbTxBufferAppend( (uint8_t *)s, i );
}

//static int HcbTxBufferAppendPrintf( const char *fmtstr, ... )
//{
//	if( _HcbTxBuffer.transmitting ) return HCB_BUFFER_BUSY;
//
//	va_list args;
//	va_start(args, fmtstr);
//	int retval = vsnprintf( (char *)&_HcbTxBuffer.buf[_HcbTxBuffer.length], (BUFFER_LEN-_HcbTxBuffer.length), fmtstr, args);
//	if( retval >= 0 ) {
//		_HcbTxBuffer.length += retval;
//	}
//
//	return retval;
//}


/// Public-facing functions

int HcbPrint( const uint8_t *data, uint8_t len )
{
	if( _HcbTxBuffer.transmitting ) return HCB_BUFFER_BUSY;

	int err = HcbTxBufferAppend( data, len );
	if( err < 0 ) return err;

	HcbTxBufferPublish();
	return HCB_BUFFER_OK;
}

int HcbPrintStr( const char *str )
{
	if( _HcbTxBuffer.transmitting ) return HCB_BUFFER_BUSY;

	int err = HcbTxBufferAppendStr( str );
	if( err < 0 ) return err;

	HcbTxBufferPublish();
	return HCB_BUFFER_OK;
}


// From:
//    https://stackoverflow.com/questions/28334435/stm32-printf-float-variable
//
//  "To enable, add -u _printf_float to your LDFLAGS."
int HcbPrintf( const char *fmtstr, ... )
{
	if( _HcbTxBuffer.transmitting ) return HCB_BUFFER_BUSY;

	va_list args;
	va_start(args, fmtstr);
	int retval = vsnprintf( (char *)_HcbTxBuffer.buf, BUFFER_LEN, fmtstr, args);
	if( retval >= 0 ) {
		_HcbTxBuffer.length = retval;
	}

	HcbTxBufferPublish();
	return HCB_BUFFER_OK;
}
