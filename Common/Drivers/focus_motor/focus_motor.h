///
/// @file  focus_motor.h
/// #brief Interface with focus motor
///

#ifndef __INVADER_FOCUS_MOTOR_H__
#define __INVADER_FOCUS_MOTOR_H__

#include <stdbool.h>
#include <stdint.h>
#include <cmsis_os.h>

#include "stm32f7xx_hal.h"


/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup FocusMotor Focus motor interface @{

enum EncoderPin {
	ENCODER_A = 0,
	ENCODER_B = 1
};

extern struct FocusMotorStats_t {
	uint32_t edges[2][2], encoderErrors;

	int32_t counts;
} focusMotorStats;

/// This state is what the focus systems _thinks_ its doing.
/// This can then be compared against hardware
extern enum FocusMotorState_t {
	FOCUS_IDLE,
	FOCUS_SEEK,
	FOCUS_JOG,
	FOCUS_TIMEOUT,
	FOCUS_LIMIT
} focusMotorState;

bool initFocusMotor( void );

void focusLimitIRQ_Callback();
void focusEncoderIRQ_Callback( enum EncoderPin, GPIO_PinState edge );

extern const osThreadDef_t os_thread_def_FocusMotor;

/// "API" commands to motor
void focusMotorJog( bool forward, uint32_t count );
void focusMotorStop( bool brake );

void focusMotorPrePowerOn();
void focusMotorPostPowerOn();

void focusMotorPrePowerOff();

void focusMotorZeroEncoder();
void focusMotorSeek();

//== Periodic task ==
#define DefaultPeriod_FocusStatus      1000
void Register_PeriodicTask_FocusStatus( int period );

/// @} @} @}

#endif
