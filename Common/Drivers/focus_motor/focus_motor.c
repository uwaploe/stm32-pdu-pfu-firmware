///
/// @file  focus_motor.h
/// #brief Interface with focus motor
///

// Logic on the control pins to the motor driver
//
//   Control_1   Control_2   Mode
//      L			L			Standby
//		H			H			Brake
//		L			H			Reverse
//		H			L			Forward

#include <stdlib.h>

#include "focus_motor.h"
#include "BSP/invader_hw.h"
#include "BSP/hw_power_enable.h"
#include "Drivers/sic_io.h"
#include "Drivers/dac_MCP4725/dac_MCP4725.h"

#include "cmsis_os.h"

#define MAX(a,b)  ( (a>b) ? a : b )

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup FocusMotor Focus motor interface @{

bool motorDeadman = false;
bool stopFocusMotorThread = false;

/// Acceptable error from target encoder value
#define FOCUS_SEEK_DEADBAND 250

#define FOCUS_THREAD_PERIOD      1

/// Counter while the system is braking.
/// Brake is released when reaches 0
/// Will be == 0 if not braking
uint32_t isBrakingCountdown;
#define FOCUS_THREAD_BRAKE_PERIOD   500  // In multiples of seek period

struct FocusMotorStats_t focusMotorStats;
enum FocusMotorState_t focusMotorState;

static const char *focusMotorStateToString() {
	switch( focusMotorState ){
	case FOCUS_IDLE:     return "IDLE";
	case FOCUS_SEEK:     return "SEEK";
	case FOCUS_JOG:      return "JOG";
	case FOCUS_TIMEOUT:  return "TIMEOUT";
	case FOCUS_LIMIT:    return "LIMIT";
	}

	return "UNK";
}

int32_t focusMotorSeekTarget, previousCounts, notMovingCount;
uint32_t countToTimeout, timeoutTarget;

void focusMotorThread(void const *argument);
osThreadId focusMotorThreadHandle = NULL;
osThreadDef(FocusMotor, focusMotorThread, osPriorityHigh, 0, configMINIMAL_STACK_SIZE * 2);


//=== Functions and constants for handling limit switch ===

uint64_t limitSwitchQueue;
#define FOCUS_LIMIT_SWITCH_WINDOW 64  // Number of limit switch samples in queue

// Number of samples before system will determine motor is not moving when it
// is expected to be moving
#define FOCUS_NOT_MOVING_LIMIT 50

//
// The limit switch is active low, so a _low_ bit count indicates that
// a limit switch has been triggered.
#define FOCUS_LIMIT_SWITCH_FROM_IDLE ( FOCUS_LIMIT_SWITCH_WINDOW-1 )
#define FOCUS_LIMIT_SWITCH_FROM_LIMIT ( 5 )

uint8_t focusSwitchLimit = FOCUS_LIMIT_SWITCH_FROM_IDLE;


// Push the latest sample into the queue
void limitSwitchQueuePush( uint8_t c ) {
	limitSwitchQueue <<= 1;
	if( c ) limitSwitchQueue |= 0x1;
}

// Counts the number of "1" (not-limited-switched) samples in queue
uint8_t limitSwitchQueueCount() {

#if FOCUS_LIMIT_SWITCH_WINDOW >= 64
	uint64_t x = limitSwitchQueue;
#else
	uint64_t x = limitSwitchQueue && (((uint64_t)0x01 << FOCUS_LIMIT_SWITCH_WINDOW) - 1);
#endif


	uint8_t c = 0;
	for (; x != 0; x &= x - 1)
		c++;
	return c;

	// GCC builtin for popcount didn't seem to be working
//	return __builtin_popcountll( (unsigned long long)limitSwitchQueue ); //&
}

// Reset the limitSwitchQueue.  Since we are looking for limit conditions,
// initialize it to be full of "not-limit-switched"
void limitSwitchReset() {
	// Basically, if you're at limit, then use a different threshold
	// Set this based on the actual hardware rather than the state
	// so that the order of (re)setting the state variable doesn't
	// matter
	//
	if( limitSwitchQueueCount() < FOCUS_LIMIT_SWITCH_FROM_IDLE ) {
		focusSwitchLimit = FOCUS_LIMIT_SWITCH_FROM_LIMIT;
	} else {
		focusSwitchLimit = FOCUS_LIMIT_SWITCH_FROM_IDLE;
	}

	limitSwitchQueue = 0xFFFFFFFFFFFFFFFFull;
}


void SicPrintFSTAT( int32_t error );


// Encoder state is stored internally as a "sequence":
//   A   B   Seq
//   0   0    0
//   1   0    1
//   1   1    2
//   0   1    3
//
static uint8_t prevEncoderSeq = 0;
static uint8_t readEncoder( void ) {
	uint8_t pinA = HAL_GPIO_ReadPin( FOCUS_GPIO_PORT, FOCUS_ENCODER_A_PIN );
	uint8_t pinB = HAL_GPIO_ReadPin( FOCUS_GPIO_PORT, FOCUS_ENCODER_B_PIN );

	return (pinA ^ pinB) | pinB << 1;
}


enum FocusMotorControl_t {
	FOCUS_MOTOR_DISABLE,
	FOCUS_MOTOR_FORWARD,
	FOCUS_MOTOR_REVERSE,
	FOCUS_MOTOR_BRAKE
};

static void setFocusMotor( enum FocusMotorControl_t mode )
{

	uint8_t pins = 0x00;

	switch( mode ) {
	case FOCUS_MOTOR_FORWARD:
		pins = 0x01;
		break;
	case FOCUS_MOTOR_REVERSE:
		pins = 0x02;
		break;
	case FOCUS_MOTOR_BRAKE:
		pins = 0x03;
		break;
	case FOCUS_MOTOR_DISABLE:
	default:
		pins = 0x00;
	}

	HAL_GPIO_WritePin( FOCUS_CONTROL_PORT, FOCUS_CONTROL_1_PIN, (pins & 0x01) ? GPIO_PIN_SET : GPIO_PIN_RESET );
	HAL_GPIO_WritePin( FOCUS_CONTROL_PORT, FOCUS_CONTROL_2_PIN, (pins & 0x02) ? GPIO_PIN_SET : GPIO_PIN_RESET );

}


// Initialize hardware
bool initFocusMotor( void )
{
	// Initialize data structures
	focusMotorState = FOCUS_IDLE;

	FOCUS_GPIO_CLK_ENABLE();
	FOCUS_CONTROL_CLK_ENABLE();

	GPIO_InitTypeDef  GPIO_InitStruct;

	// Configure inputs
	GPIO_InitStruct.Pin = FOCUS_LIMIT_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	//GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(FOCUS_GPIO_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = FOCUS_ENCODER_A_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	HAL_GPIO_Init(FOCUS_GPIO_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = FOCUS_ENCODER_B_PIN;
	HAL_GPIO_Init(FOCUS_GPIO_PORT, &GPIO_InitStruct);

	// Limit switch input on Pin 9
	//AL_NVIC_SetPriority(FOCUS_LIMIT_IRQ, 2, 0);
	//HAL_NVIC_EnableIRQ(FOCUS_LIMIT_IRQ);

	// With encoder inputs on PINs 14 and 15, both map to EXTI15_10_IRQn
	HAL_NVIC_SetPriority(FOCUS_ENCODER_IRQ, 2, 0);
	//HAL_NVIC_EnableIRQ(FOCUS_ENCODER_IRQ);

	// Configure output pins
	GPIO_InitStruct.Pin = FOCUS_CONTROL_1_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init(FOCUS_CONTROL_PORT, &GPIO_InitStruct);
	HAL_GPIO_WritePin( FOCUS_CONTROL_PORT, FOCUS_CONTROL_1_PIN, GPIO_PIN_RESET );

	GPIO_InitStruct.Pin = FOCUS_CONTROL_2_PIN;
	HAL_GPIO_Init(FOCUS_CONTROL_PORT, &GPIO_InitStruct);
	HAL_GPIO_WritePin( FOCUS_CONTROL_PORT, FOCUS_CONTROL_2_PIN, GPIO_PIN_RESET );

	limitSwitchReset();
	focusMotorState = FOCUS_IDLE;

	SicPrintStr("!!       Focus motor : Initialized\r\n");
	return true;
}

void focusEncoderIRQ_Callback( enum EncoderPin pin, GPIO_PinState edge  )
{

	focusMotorStats.edges[pin][edge]++;

	GPIO_PinState pinA, pinB;

	if( pin == ENCODER_A ) {
		pinA = edge;
		pinB = HAL_GPIO_ReadPin( FOCUS_GPIO_PORT, FOCUS_ENCODER_B_PIN );
	} else {
		pinA = HAL_GPIO_ReadPin( FOCUS_GPIO_PORT, FOCUS_ENCODER_A_PIN );
		pinB = edge;
	}

	const uint8_t seq = (pinA ^ pinB) | pinB << 1;
	const uint8_t delta = (seq - prevEncoderSeq) & 0x03;

	if( delta == 1 ) {
		focusMotorStats.counts++;
	} else if( delta == 3) {
		focusMotorStats.counts--;
	} else {
		// Either delta = 0 or 2 is an unexpected transition
		focusMotorStats.encoderErrors++;
	}


	prevEncoderSeq = seq;
}

void focusMotorPrePowerOn()
{
	focusMotorStop( false );
}

void focusMotorPostPowerOn()
{
	// There is an initial transient after the power is enabled
	osDelay(50);

	// Initialize the current state of the encoder
	prevEncoderSeq = readEncoder();
	motorDeadman = false;

	// Clear any pending encoder interrupts
	HAL_NVIC_DisableIRQ(FOCUS_ENCODER_IRQ);
	while (HAL_NVIC_GetPendingIRQ(FOCUS_ENCODER_IRQ)) {
		__HAL_GPIO_EXTI_CLEAR_IT(FOCUS_ENCODER_A_PIN);
		__HAL_GPIO_EXTI_CLEAR_IT(FOCUS_ENCODER_B_PIN);
		HAL_NVIC_ClearPendingIRQ(FOCUS_ENCODER_IRQ);
	}
	HAL_NVIC_EnableIRQ(FOCUS_ENCODER_IRQ);

	limitSwitchReset();
	focusMotorState = FOCUS_IDLE;
	previousCounts = focusMotorStats.counts;

	// Initialize high-speed focus motor thread
	stopFocusMotorThread = false;
	focusMotorThreadHandle = osThreadCreate( osThread(FocusMotor), NULL);
	if( focusMotorThreadHandle == NULL ) {
		SicPrintStr("!! ** Unable to initialize focus motor thread, SEEK will not work");
	}
}


void focusMotorPrePowerOff()
{
	focusMotorStop( false );
	stopFocusMotorThread = true;

	HAL_NVIC_DisableIRQ(FOCUS_ENCODER_IRQ);
}



void focusMotorThread(void const * argument)
{
	uint8_t loopCount = 0;

	while( stopFocusMotorThread == false ) {

		motorDeadman = true;

		// Check that motor is moving
		const int32_t countsNow = focusMotorStats.counts;
		const int32_t delta = countsNow - previousCounts;
		bool isMoving = abs(focusMotorStats.counts - previousCounts) > 10;
		previousCounts = countsNow;
		if( !isMoving ) notMovingCount++;

		// Put a multiplier on limit switch checking
		// to preserve 64bit dynamic range
		if( (loopCount++ % 2) == 0 ) {

			/// Limit switch checking
			uint8_t limit = HAL_GPIO_ReadPin( FOCUS_GPIO_PORT, FOCUS_LIMIT_PIN );
			limitSwitchQueuePush( limit );

			// If for some reason the limit isn't set not set, set it
			if( (focusSwitchLimit != FOCUS_LIMIT_SWITCH_FROM_IDLE) &&
				(focusSwitchLimit != FOCUS_LIMIT_SWITCH_FROM_LIMIT) ) {
					focusSwitchLimit = FOCUS_LIMIT_SWITCH_FROM_IDLE;
				}

			if( limitSwitchQueueCount() < focusSwitchLimit ) {

				/// Only brake when transitioning into the LIMIT state...
				focusMotorStop( focusMotorState != FOCUS_LIMIT );
				focusMotorState = FOCUS_LIMIT;
				goto loop;
			}

		}

		if( focusMotorState == FOCUS_JOG ) {

			if( notMovingCount > FOCUS_NOT_MOVING_LIMIT ) {
				focusMotorStop( true );
				focusMotorState = FOCUS_TIMEOUT;
				continue;
			}

			// Repetitive code, but timeout while jogging
			// isn't a fault, so it has a different state transition
			if( countToTimeout++ >= timeoutTarget  ) {
				focusMotorStop( true );
				focusMotorState = FOCUS_IDLE;
				continue;
			}

		} else if( focusMotorState == FOCUS_SEEK ) {

			if( notMovingCount > FOCUS_NOT_MOVING_LIMIT ) {
				focusMotorStop( true );
				focusMotorState = FOCUS_TIMEOUT;
				continue;
			}

			if( countToTimeout++ >= timeoutTarget  ) {
				focusMotorStop( true );
				focusMotorState = FOCUS_TIMEOUT;
				continue;
			}

			const int32_t counts = focusMotorStats.counts;
			const int32_t error = focusMotorSeekTarget - counts;

			// Status message every 128 cycles
			if( (countToTimeout & 0x7F) == 0 ) {
				SicPrintFSTAT(error);
			}

			if( abs(error) < FOCUS_SEEK_DEADBAND ) {
				// Brake the motor
				focusMotorStop( true );
				focusMotorState = FOCUS_IDLE;
				continue;

			} else if( error > 0 ) {
				setFocusMotor( FOCUS_MOTOR_FORWARD );
			} else {
				setFocusMotor( FOCUS_MOTOR_REVERSE );
			}

		} else if( isBrakingCountdown > 0 && isBrakingCountdown-- == 0 ) {
			// Transition from brake to just idle
			focusMotorStop( false );
		}
loop:
		osDelay(FOCUS_THREAD_PERIOD);
	}

	motorDeadman = false;
	osThreadTerminate(NULL);
}


// Handle all prep common to either a seek or a jog
static void prepForMotion( uint32_t timeout ) {
	limitSwitchReset();
	notMovingCount = 0;
	countToTimeout = 0;
	timeoutTarget = timeout;
}

/// "API" commands to the focus motor
void focusMotorJog( bool forward, uint32_t ms )
{

	if( ! motorDeadman ) {
		SicPrintStr("!!! motorDeadman not set.  This should never happen. Refusing to move");
		return;
	}

	prepForMotion( ms );
	focusMotorState = FOCUS_JOG;


	if( forward ) {
		setFocusMotor( FOCUS_MOTOR_FORWARD );
	} else {
		setFocusMotor( FOCUS_MOTOR_REVERSE );
	}

}

void focusMotorSeek( uint32_t counts )
{
	if( ! motorDeadman ) {
		SicPrintStr("!!! motorDeadman not set.  This should never happen. Refusing to move");
		return;
	}

	focusMotorState = FOCUS_SEEK;
	focusMotorSeekTarget = counts;

	// TIMEOUT is enumerated in focusMotorThread calls, which are currently ~1000Hz
	// Estimated turn rate is 1000 counts per second, so set timeout to counts / 16
	// or 1 sec, whichever is larger
	const int32_t error = abs(counts - focusMotorStats.counts);
	timeoutTarget = MAX(error >> 4, 1000);

	prepForMotion(timeoutTarget);
}

void focusMotorStop( bool brake )
{
	if( brake ) {
		setFocusMotor( FOCUS_MOTOR_BRAKE );
		isBrakingCountdown = FOCUS_THREAD_BRAKE_PERIOD;
	} else {
		setFocusMotor( FOCUS_MOTOR_DISABLE );
		isBrakingCountdown = 0;
	}
}

void focusMotorZeroEncoder()
{
	focusMotorStats.counts = 0;
}

//=========================== Focus motor messages ==================================

void SicPrintFSTAT( int32_t error ) {
	SicNMEAPrintf("FSTAT", "%s,%s,%d,%d",
			focusMotorStateToString(),
			(isBrakingCountdown > 0 ? "BRAKE" : ""),
			focusMotorStats.counts,
			error );
}


/// \brief
///
/// Reports on current status of focus DAC (if enabled)
///
static void PeriodicTask_FocusStatus( void const *arg )
{
	if( powerState( FOCUS_EN ) ) {
		uint16_t dacValue = dacMCPRead( FOCUS_DAC_ADDRESS );

		SicNMEAPrintf("FDBUG", "%d,%d,%d,%d,%d,%03X",
								focusMotorStats.edges[ENCODER_A][GPIO_PIN_RESET],
								focusMotorStats.edges[ENCODER_A][GPIO_PIN_SET],
								focusMotorStats.edges[ENCODER_B][GPIO_PIN_RESET],
								focusMotorStats.edges[ENCODER_B][GPIO_PIN_SET],
								focusMotorStats.encoderErrors,
								dacValue );

		SicPrintFSTAT(0);
	}

}

void Register_PeriodicTask_FocusStatus( int period )
{
	  osTimerDef(periodicFocusStatus, PeriodicTask_FocusStatus );
	  osTimerId periodicFocusStatusTimer = osTimerCreate(osTimer(periodicFocusStatus), osTimerPeriodic, (void *)NULL);
	  osTimerStart(periodicFocusStatusTimer, period);
}



/// @} @} @}



