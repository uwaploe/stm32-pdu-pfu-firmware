///
/// @file  laser_control.c
/// #brief Laser subsystem control
///

#include "laser_control.h"
#include "BSP/hw_power_enable.h"

#include "Drivers/adc_LTC1867/adc_periodic_task.h"
#include "Drivers/dac_DAC8562/dac_DAC8562.h"

#include "Drivers/sic_io.h"

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup LaserControl Laser subsystem @{

static void Register_PeriodicTask_LaserDAC( int period );
static void PeriodicTask_LaserDAC( void const *arg );

//=== Timer for periodic task which reports LaserDAC value
#define LaserDACState_DefaultPeriod      5000
static void Register_PeriodicTask_LaserDACStatus( int period );
static void PeriodicTask_LaserDACStatus( void const *arg );

osTimerDef(periodicLaserDAC, PeriodicTask_LaserDACStatus );
osTimerId _periodicLaserDACTimer;



// Global status structure
struct LaserStatus_t LaserStatus = { LASER_OFF, 0 };


/// \brief Callback function for laser subsystem watchdog
static void LaserWatchdogThread(void const * argument);

osThreadId watchdogThreadId = NULL;
osThreadDef(LaserWatchdog, LaserWatchdogThread, osPriorityHigh, 0, configMINIMAL_STACK_SIZE * 2);

bool isLaserMode() {
	return ( LaserStatus.status == LASER_ON );
}

LaserStatus_t enableLaserMode() {

	// Always reset timeout...
	LaserStatus.watchdogCountdown = LaserWatchdogTimeout;

	if( isLaserMode() ) { return LaserStatus.status; }

	// Ensure secondary circuits are off
	powerOff( LASER_ISO_12 );
	powerOff( LASER_ISO_28 );
	powerOn( LASER_ISO_5 );

	// Wait for power to settle?
	osDelay( 100 );

	// Enable watchdog
	watchdogThreadId = osThreadCreate( osThread(LaserWatchdog), NULL );
	if( watchdogThreadId == NULL ) {
		// Error condition, go no further
		return LASER_ERR_WATCHDOG_FAIL;
	}

	// Enable ADC read
	Register_PeriodicTask_ADCSampling( LTC_ADC_LASER, DefaultPeriod_ADCSampling );

	Register_PeriodicTask_LaserDACStatus( LaserDACState_DefaultPeriod );

	// Check for input laser 24V .. something won't work if it's not supplied


	LaserStatus.status = LASER_ON;

	return LaserStatus.status;
}




// Put the common "shut down the hardware" code here
static LaserStatus_t shutdownLaser() {

	osTimerStop( _periodicLaserDACTimer );
	Halt_PeriodicTask_ADCSampling( LTC_ADC_LASER );

	powerOff( LASER_ISO_12 );
	powerOff( LASER_ISO_28 );
	powerOff( LASER_ISO_5 );

	return LASER_OFF;
}

LaserStatus_t disableLaserMode() {

	LaserStatus_t err = shutdownLaser();

	if( (osThreadGetId() != watchdogThreadId) && (watchdogThreadId != NULL) ) {
		osThreadTerminate( watchdogThreadId );
		watchdogThreadId = NULL;
	}

	LaserStatus.status = err;
	return LaserStatus.status;
}



// Laser watchdog runs in an independent thread
static void LaserWatchdogThread(void const * argument)
{
  osStatus status = osOK;

  // \TODO.  Different paths depending on the type of event?
  while( true ) {
	  status = osDelay( 100 );

	  LaserStatus.watchdogCountdown--;
	  if( LaserStatus.watchdogCountdown <= 0 ) break;
  }

  //
  shutdownLaser();
  LaserStatus.status = LASER_WATCHDOG_TIMEOUT;

  SicNMEAPrintf("$LTIME","");

  osThreadTerminate(NULL);
}



///
/// Reports on current status of laser DAC (if enabled)
///
static void PeriodicTask_LaserDACStatus( void const *arg )
{
	if( powerState( LASER_ISO_5 ) ) {

		uint16_t value = dac8562Get();
		SicNMEAPrintf("LDAC", "%03X", value );
	}

//	   static uint16_t value = 0;
//	   dac8562Init();
//	   dac8562Set( value );
//	   value += 0x0100;
}

void Register_PeriodicTask_LaserDACStatus( int period )
{
	_periodicLaserDACTimer = osTimerCreate(osTimer(periodicLaserDAC), osTimerPeriodic, (void *)NULL);
	osTimerStart( _periodicLaserDACTimer, period);
}

/// @} @} @}


