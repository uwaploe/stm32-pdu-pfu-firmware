///
/// @file  laser_control.h
/// #brief Laser subsystem control
///

#ifndef __INVADER_LASER_CONTROL_H__
#define __INVADER_LASER_CONTROL_H__

#include <stdbool.h>
#include <stdint.h>

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup LaserControl Laser subsystem @{


typedef uint32_t LaserStatus_t;

// Watchdog runs at 10Hz, specify timeout in second*10
#ifdef DEBUG
static const int32_t LaserWatchdogTimeout = 36000;
#else
static const int32_t LaserWatchdogTimeout = 50;
#endif

struct LaserStatus_t {
	LaserStatus_t status;
	int32_t       watchdogCountdown;
};

extern struct LaserStatus_t LaserStatus;

#define LASER_ON				 1
#define LASER_OFF 				 0
#define LASER_WATCHDOG_TIMEOUT  -1
#define LASER_ERR_WATCHDOG_FAIL -100

LaserStatus_t enableLaserMode();
LaserStatus_t disableLaserMode();

bool isLaserMode();

/// @} @} @}

#endif
