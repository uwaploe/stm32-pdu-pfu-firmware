///
/// @file  sic_io.h
/// #brief Functions for interfacing with the SIC
///

#include "sic_io.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "Drivers/global_state.h"

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup SicIO SIC Input/Output @{

osMessageQDef(InputMsgBox, 2, uint8_t );              // Define message queue
osMessageQId  InputMsgBox;

// Tx buffers
SicTxBuffer_t _sicTxBuffers[NUM_BUFFERS];

// Rx buffer
SicRxBuffer_t _sicRxBuffer;


static uint8_t nmea0183_checksum(char *nmea_data)
{
	uint8_t crc = 0;
    size_t i;

    // Skip the first charaater
    const size_t len = strlen(nmea_data);
    for(i = 1; i < len; i++) {
        crc ^= nmea_data[i];
    }

    return crc;
}

void SicBuffersInitialize( void )
{
	for( uint8_t i = 0; i < NUM_BUFFERS; ++i ) {
		SicBufferRelease( i );
	}

  // Initialize the RX buffer
  _sicRxBuffer.length = 0;
  InputMsgBox = osMessageCreate(osMessageQ(InputMsgBox), NULL);  // create msg queue
}

SicBufferId_t SicBufferReserve( void )
{
	for( uint8_t i = 0; i < NUM_BUFFERS; ++i ) {
		if( _sicTxBuffers[i].reserved == false ) {

			_sicTxBuffers[i].reserved = true;
			_sicTxBuffers[i].readyToTx = false;
			_sicTxBuffers[i].length = 0;

			return i;
		}
	}

	ErrorCounters.ioBufferUnavailable++;
	return SIC_BUFFER_ERROR;
}




int SicBufferAppend( SicBufferId_t idx, const uint8_t *data, uint8_t len )
{
	if( idx >= NUM_BUFFERS ) return SIC_BUFFER_ERROR;
	if( !_sicTxBuffers[idx].reserved || _sicTxBuffers[idx].readyToTx ) return SIC_BUFFER_ERROR;

	if( (len + _sicTxBuffers[idx].length) > BUFFER_LEN ) return SIC_BUFFER_ERROR;

	for( uint8_t i = 0; i < len; i++ ) {
		_sicTxBuffers[idx].buf[ _sicTxBuffers[idx].length + i ] = data[i];
	}

	_sicTxBuffers[idx].length += len;

	return _sicTxBuffers[idx].length;
}

int SicBufferAppendStr( SicBufferId_t idx, const char *s )
{
	size_t i = 0;
	while( s[i] && i < BUFFER_LEN ) { i += 1; }

	if( i >= BUFFER_LEN ) return SIC_BUFFER_ERROR;

	return SicBufferAppend( idx, (uint8_t *)s, i );
}

int SicBufferAppendPrintf( SicBufferId_t idx, const char *fmtstr, ... )
{
	if( idx >= NUM_BUFFERS ) return SIC_BUFFER_ERROR;

	va_list args;
	va_start(args, fmtstr);
	int retval = vsnprintf( (char *)&_sicTxBuffers[idx].buf[_sicTxBuffers[idx].length], (BUFFER_LEN-_sicTxBuffers[idx].length), fmtstr, args);
	if( retval >= 0 ) {
		_sicTxBuffers[idx].length += retval;
	}

	return retval;
}

void SicBufferFinalize( SicBufferId_t idx )
{
	if( idx >= NUM_BUFFERS ) return;
	if( _sicTxBuffers[idx].length == 0 ) return;

	_sicTxBuffers[idx].readyToTx = true;

	// Send to the hardware BSP
	SicBufferPublish( idx );
}

int SicBufferFinalizeNMEA( SicBufferId_t idx ) {
	int checksum = nmea0183_checksum( _sicTxBuffers[idx].buf );

	int err = SicBufferAppendPrintf( idx, "*%02X\r\n", checksum );
	if( err < 0 ) return err;

	SicBufferFinalize( idx );
	return 0;
}


void SicBufferRelease( SicBufferId_t idx ) {
	if( idx >= NUM_BUFFERS ) return;

	_sicTxBuffers[idx].readyToTx = false;
	_sicTxBuffers[idx].reserved = false;
	_sicTxBuffers[idx].length = 0;
}



SicBufferId_t SicNextReadyBuffer( void ) {

  for( uint8_t i = 0; i < NUM_BUFFERS; ++i ) {
	  if( _sicTxBuffers[i].readyToTx ) {
		  return i;
	  }
  }

  return SIC_NO_BUFFER_READY;
}


//===================================================================
//   All-at-once functions
//===================================================================


int SicPrint( const uint8_t *data, uint8_t len )
{
	SicBufferId_t idx = SicBufferReserve();
	if( idx < 0 ) return idx;

	int err = SicBufferAppend( idx, data, len );
	if( err < 0 ) return err;

	SicBufferFinalize( idx );

	return SIC_BUFFER_OK;
}

int SicPrintStr( const char *str )
{
	SicBufferId_t idx = SicBufferReserve();
	if( idx < 0 ) return idx;

	int err = SicBufferAppendStr( idx, str );
	if( err < 0 ) return err;

	SicBufferFinalize( idx );

	return SIC_BUFFER_OK;
}


// From:
//    https://stackoverflow.com/questions/28334435/stm32-printf-float-variable
//
//  "To enable, add -u _printf_float to your LDFLAGS."
int SicPrintf( const char *fmtstr, ... )
{
	SicBufferId_t idx = SicBufferReserve();
	if( idx < 0 ) return idx;

	va_list args;
	va_start(args, fmtstr);
	int retval = vsnprintf( (char *)_sicTxBuffers[idx].buf, BUFFER_LEN, fmtstr, args);
	if( retval >= 0 ) {
		_sicTxBuffers[idx].length = retval;
	}

	SicBufferFinalize( idx );

	return retval;
}


int SicNMEAPrint( const char *tag, const uint8_t *data, uint8_t len )
{
	const int totalLen = 8 + strlen(tag) + len;
	int err = 0;

	if( totalLen > BUFFER_LEN ) return SIC_BUFFER_OVERRUN;


	uint8_t idx = SicBufferReserve();
	if( idx < 0 ) return idx;

	err = SicBufferAppendPrintf( idx, "$%5s,", tag );
	if( err < 0 ) goto fail;

	err = SicBufferAppend( idx, data, len );
	if( err < 0 ) goto fail;

	return SicBufferFinalizeNMEA( idx );

fail:

	SicBufferRelease( idx );
	return err;
}


int SicNMEAPrintf( const char *tag, const char *fmtstr, ... )
{
	const int Reserved = 8 + strlen(tag);   // "$ABCDE," (8 chars) + "*AA\r\n" (5 chars)
	int err;

	uint8_t idx = SicBufferReserve();
	if( idx < 0 ) return idx;

	err = SicBufferAppendPrintf( idx, "$%s,", tag );
	if( err < 0 ) goto fail;

	va_list args;
	va_start(args, fmtstr);
	int retval = vsnprintf( (char *)&_sicTxBuffers[idx].buf[_sicTxBuffers[idx].length], (BUFFER_LEN-_sicTxBuffers[idx].length-Reserved), fmtstr, args);
	if( retval >= 0 ) {
		_sicTxBuffers[idx].length += retval;
	}


	return SicBufferFinalizeNMEA( idx );

fail:

	SicBufferRelease( idx );
	return err;

}




/// @} @} @}
