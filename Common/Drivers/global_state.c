///
/// @file  global_state.c
/// #brief Global state variables
///

#include "global_state.h"


/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup GlobalState Global state variables @{

struct GlobalState_t GlobalState;
struct ErrorCounter_t ErrorCounters;

void initGlobalState() {

	  __HAL_RCC_RTC_CLK_ENABLE();
	  //LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);
	  HAL_PWR_EnableBkUpAccess();
	  //LL_PWR_EnableBkUpAccess();

	// Read an update boot count;
	GlobalState.bootCount = LL_RTC_BAK_GetRegister( RTC, BACKUP_REG_BOOT_COUNT );
	GlobalState.bootCount++;
	LL_RTC_BAK_SetRegister( RTC, BACKUP_REG_BOOT_COUNT, GlobalState.bootCount );

	GlobalState.uptime = 0;


	ErrorCounters.ioBufferUnavailable = 0;
}


bool errorCountersNonZero() {
	return (ErrorCounters.ioBufferUnavailable > 0);
}

/// @} @} @}
