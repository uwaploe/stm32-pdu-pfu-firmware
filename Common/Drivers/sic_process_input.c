///
/// @file  sic_process_input.h
/// #brief Processes incoming SIC commands
///

#include <string.h>
#include <stdlib.h>

#include "Drivers/hcb_uart/hcb_uart.h"
#include "BSP/hw_uart_sic.h"
#include "BSP/hw_power_enable.h"

#include "sic_io.h"

#include "BSP/hw_power_enable.h"

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup SicIO SIC Input/Output @{

osThreadDef(ProcessSicInput, ProcessSicInputThread, osPriorityLow, 0, configMINIMAL_STACK_SIZE * 2);

void ProcessSicInputThread(void const * argument)
{

	while(1) {

		osEvent event = osMessageGet( InputMsgBox, osWaitForever );

		if( event.status == osEventMessage ) {
			//uint8_t val = event.value.v;

			// Quickly grab a copy of incoming message
			//  -- assumes this happens before the next message come from the SIC
			SicRxBuffer_t msg;
			msg.length = _sicRxBuffer.length;
			memcpy( msg.buf, _sicRxBuffer.buf, msg.length );

			// Terminate message
			msg.buf[ msg.length ] = '\0';

			processSicInput( &msg );
		}

  }

	osThreadTerminate(NULL);
}

// Input-processing commands which are common to multiple
// hardware platforms.
//
// Must be called from the platform-specific processSicInput()


void sicProcessPowerEnable( char *strtok_state )
{
	SicBufferId_t outBuf = SicBufferReserve();

	SicBufferAppendStr( outBuf, "$PWREN" );

	char *circuit, *status;
	while( (circuit = strtok_r( NULL, ",*", &strtok_state)) ) {

		// Check if we recognize circuit?
		enum PowerEnable powerEnable = findPowerEnable(circuit );
		if( powerEnable == POWER_ENABLE_NOT_FOUND ) {
			SicBufferAppendStr( outBuf, ",ERR");
			goto done;
		}

		SicBufferAppendStr( outBuf, "," );
		SicBufferAppendStr( outBuf, powerEnableName( powerEnable ) );


		status = strtok_r( NULL, ",*", &strtok_state );
		if( status == NULL ) {
			SicBufferAppendStr( outBuf, ",ERR");
			goto done;
		}

		// \TODO.  More sophisticated parser
		int value = atoi( status );

		if( value == 1 ) {

//			if( powerEnable == FOCUS_EN ) {
//				focusMotorPrePowerOn();
//			}

			powerOn( powerEnable );

//			if( powerEnable == FOCUS_EN ) {
//				focusMotorPostPowerOn();
//			}

		} else if( value == 0 ) {

//			if( powerEnable == FOCUS_EN ) {
//				focusMotorPrePowerOff();
//			}

			powerOff( powerEnable );



		} else {
			SicBufferAppendStr( outBuf, ",ERR");
			goto done;
		}

		SicBufferAppendPrintf( outBuf, ",%d", powerState( powerEnable ) );
	}

done:

	SicBufferFinalizeNMEA( outBuf );


}

/// @} @} @}

