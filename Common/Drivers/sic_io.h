///
/// @file  sic_io.h
/// #brief Functions for interfacing with the SIC
///
/// Implements the buffers


#ifndef __INVADER_SIC_IO_H__
#define __INVADER_SIC_IO_H__

#include <stdint.h>
#include <stdbool.h>

#include <cmsis_os.h>

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup SicIO SIC Input/Output @{

/// Buffering
#define NUM_BUFFERS 16
#define BUFFER_LEN  256

typedef struct SicTxBuffer {
	char buf[BUFFER_LEN];
	uint8_t length;
	bool    readyToTx, reserved;
} SicTxBuffer_t;

extern SicTxBuffer_t _sicTxBuffers[NUM_BUFFERS];

typedef struct SicRxBuffer {
	char buf[BUFFER_LEN];
	uint8_t length;
	char *parserCursor;
} SicRxBuffer_t;

extern SicRxBuffer_t _sicRxBuffer;

// Semaphore used any time a complete new message has arrived
extern osMessageQId  InputMsgBox;

// Buffer interface function error codes
#define SIC_BUFFER_OK 		0
#define SIC_BUFFER_OVERRUN  -125
#define SIC_NO_BUFFER_READY -126
#define SIC_BUFFER_ERROR 	-127

typedef int8_t SicBufferId_t;


/// Initializes the SicBuffer data structures
///
void SicBuffersInitialize( void );

/// Reserves one of the SicBuffer static-allocated buffers for use.
///
/// When done, the buffer __must__ be returned with either SicBufferFinalize of SicBufferRelease.
///
/// \return SicBufferId_t  A handle to one of the SicBuffers
///
SicBufferId_t SicBufferReserve();
void SicBufferFinalize( SicBufferId_t idx );
int SicBufferFinalizeNMEA( SicBufferId_t idx );
void SicBufferRelease( SicBufferId_t idx );

SicBufferId_t SicNextReadyBuffer();



// Functions for writing to a SicBuffer
int SicBufferAppend(    SicBufferId_t idx, const uint8_t *data, uint8_t len );
int SicBufferAppendStr( SicBufferId_t idx, const char *str );
int SicBufferAppendPrintf( SicBufferId_t idx, const char *fmtstr, ... );


// More structures functions for interfacing with Sic Buffers
int SicPrint( const uint8_t *data, uint8_t len );
int SicPrintStr( const char *str );
int SicPrintf( const char *fmtstr, ... );

int SicNMEAPrint(  const char *tag, const uint8_t *data, uint8_t len );
int SicNMEAPrintf( const char *tag, const char *fmtstr, ... );


// Abstract functions which must be provided by the hardware interface
void SicBufferPublish( SicBufferId_t idx );


///== Thread for processing SIC input ===

void ProcessSicInputThread(void const * argument);
extern const osThreadDef_t os_thread_def_ProcessSicInput;

// This function must be provided by the end user
void processSicInput( SicRxBuffer_t *msg );

// Input-processing functions which are common to the PDU and PFU
void sicProcessPowerEnable( char *strok_state );


/// @} @} @}



#endif
