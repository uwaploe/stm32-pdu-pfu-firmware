///
///  Testing-time functions for validating data structures
///

#ifndef __INVADER_BSP_HW_VALIDATE_H__
#define __INVADER_BSP_HW_VALIDATE_H__

#ifdef RELEASE
#error "Validation should not be included in RELEASE builds.  Please exclude from that configuration."
#endif


#include <stdbool.h>

bool validate( void );

#endif
