///
///
///

#include "BSP/invader_hw_validate.h"
#include "BSP/invader_hw.h"
#include <assert.h>

#include <stdio.h>


#ifdef RELEASE

#error "Validation should not be compiled in RELEASE builds.  Please exclude from that configuration."

#endif


static bool validateLTCADCTable( void ) {

	assert( numLtcAdcs == NUM_LTC_ADCS );

	for( int i = 0 ; i < NUM_LTC_ADCS ; i++ ) {
		assert( ltcAdcs[ i ].adcIdx == i );
	}

	return true;
}

static bool validateSPIChipSelects( void ) {

	if( numSpiChipSelects != NUM_SPI_CHIP_SELECT ) return false;

	for( int i = 0; i < NUM_SPI_CHIP_SELECT; ++i  ) {
		if( _spiChipSelects[i].csIdx != i ) return false;
	}

	return true;
}

static bool validatePowerEnables( void ) {

	assert( numPowerEnables == NUM_POWER_ENABLES );

	// I have to do this manually?
	for( int i = 0 ; i < NUM_POWER_ENABLES ; i++ ) {
		assert( powerEnables[ i ].psIdx == i );
	}

	return true;
}



bool validate( void ) {
	if( !validateLTCADCTable() )   return false;
	if( !validatePowerEnables() )  return false;
	if( !validateSPIChipSelects() ) return false;

	return true;
}


