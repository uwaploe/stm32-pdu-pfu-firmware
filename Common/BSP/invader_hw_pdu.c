///
///

#include "hw_spi_sys.h"

#include "Drivers/focus_motor/focus_motor.h"

//===================================================================
//        System SPI bus
//===================================================================


const struct SPIBusChipSelect _spiChipSelects[] = {
	{ SPI_IMU_AG,     GPIOF, GPIO_PIN_6,  3, SPI_NEEDS_3TO5_TRISTATE },       // LSM9DS1_Accel/Gyro
	{ SPI_IMU_M,      GPIOF, GPIO_PIN_7,  3, SPI_NEEDS_3TO5_TRISTATE },	    // LSM9DS1_Mag
	{ SPI_BME280,     GPIOA, GPIO_PIN_5,  3, SPI_NEEDS_3TO5_TRISTATE },       // BME280
	{ SPI_ADC0,       GPIOB, GPIO_PIN_2,  0, SPI_NOT_SPECIAL },       // LTC1867 #0
	{ SPI_ADC1,       GPIOF, GPIO_PIN_11, 0, SPI_NOT_SPECIAL },       // LTC1867 #1
	{ SPI_LASER_CS,   GPIOA, GPIO_PIN_6,  0, SPI_NOT_SPECIAL },       // LASER CS
	{ SPI_LASER_SSA0, GPIOB, GPIO_PIN_0,  0, SPI_NOT_SPECIAL },       // LASER CS mux 0
	{ SPI_LASER_SSA1, GPIOB, GPIO_PIN_1,  0, SPI_NOT_SPECIAL },       // LASER CS mux 1
	{ SPI_LASER_ADC,  GPIOG, GPIO_PIN_0,  0, SPI_ON_LASER_MUX, 0x00 },  // LASER ADC0
	{ SPI_LASER_DAC,  GPIOG, GPIO_PIN_0,  2, SPI_ON_LASER_MUX, 0x01 },  // Target laser DAC
	{ SPI_PDUB_ADC1,  GPIOF, GPIO_PIN_15, 0, SPI_NOT_SPECIAL },
	{ SPI_PDUB_ADC2,  GPIOB, GPIO_PIN_10, 0, SPI_NOT_SPECIAL },
	{ SPI_PDUB_ADC3,  GPIOE, GPIO_PIN_15, 0, SPI_NOT_SPECIAL },
	{ SPI_PDUB_ADC4,  GPIOB, GPIO_PIN_12, 0, SPI_NOT_SPECIAL },
	{ SPI_CS_3TO5_TRISTATE, GPIOG, GPIO_PIN_1, 0, SPI_NOT_SPECIAL },     // Special additional pin required to enable/disable tristate between 3 and 5V SPI buses
};

const int numSpiChipSelects = sizeof( _spiChipSelects ) / sizeof( struct SPIBusChipSelect );

//=== SPI device interface functions ===

struct Ltc1867Entry ltcAdcs[] = {
	{ LTC_ADC0, SPI_ADC0, 0 },		    // ADC0 on PDU-A (temperature probes)
	{ LTC_ADC1, SPI_ADC1, 0 },		    // ADC1 on APU-A (SYS_3,5,12V I and V)
	{ LTC_ADC2, SPI_PDUB_ADC1, 0 },
	{ LTC_ADC3, SPI_PDUB_ADC2, 0 },
	{ LTC_ADC4, SPI_PDUB_ADC3, 0 },
	{ LTC_ADC5, SPI_PDUB_ADC4, 0 },
	{ LTC_ADC_LASER, SPI_LASER_ADC, 0 } // Isolated ADC on laser bus (laser V and I)
};

const int numLtcAdcs = sizeof( ltcAdcs ) / sizeof( struct Ltc1867Entry );


//== Power switch GPIOs ==

/// Strong assumption that all undefined function pointers are set to NULL?
struct PowerEnableEntry powerEnables[] = {
	{ TB_EN, "TB", GPIOD, GPIO_PIN_8, false,       NULL, NULL, NULL, NULL },
	{ UV_EN, "UV", GPIOG, GPIO_PIN_5, false,       NULL, NULL, NULL, NULL },
	{ BEXP_EN, "BEXP", GPIOA, GPIO_PIN_9, false,   NULL, NULL, NULL, NULL },
	{ CCD_EN, "CCD", GPIOC, GPIO_PIN_6, false,     NULL, NULL, NULL, NULL },
	{ HCB_EN, "HCB" ,GPIOC, GPIO_PIN_7, true,      NULL, NULL, NULL, NULL },
	{ HCB_MODE, "HCBMODE", GPIOB, GPIO_PIN_15, false, NULL, NULL, NULL, NULL },
	{ CAM1_EN, "CAM1", GPIOG, GPIO_PIN_8, false,   NULL, NULL, NULL, NULL },
	{ CAM2_EN, "CAM2", GPIOG, GPIO_PIN_7, false,   NULL, NULL, NULL, NULL },
	{ LED1_EN, "LED1", GPIOA, GPIO_PIN_8, false,   NULL, NULL, NULL, NULL },
	{ LED2_EN, "LED2", GPIOC, GPIO_PIN_8, false,   NULL, NULL, NULL, NULL },
	{ EXPAN_EN, "EXPAN", GPIOG, GPIO_PIN_6, false, NULL, NULL, NULL, NULL },
	{ FOCUS_EN, "FOCUS", GPIOA, GPIO_PIN_10, false, focusMotorPrePowerOn, focusMotorPostPowerOn, focusMotorPrePowerOff, NULL },
	{ LASER_ISO_5, "", GPIOF, GPIO_PIN_8, false,   NULL, NULL, NULL, NULL },
	{ LASER_ISO_28, "", GPIOF, GPIO_PIN_9, false,  NULL, NULL, NULL, NULL },
	{ LASER_ISO_12, "", GPIOF, GPIO_PIN_10, false, NULL, NULL, NULL, NULL },
};

const int numPowerEnables = sizeof( powerEnables ) / sizeof(struct PowerEnableEntry);



/// Clock configuration specific to the PDU
//
bool dac8562SpiTransfer( uint8_t cmd, uint16_t data ) {
	uint8_t d[2] = { (data&0xFF00)>>8, (data&0x00FF) };

	Sys_SPI_WriteDataAddr( SPI_LASER_DAC, cmd, d, 2 );

	return true;
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* \todo Define rational error handler... */
  while(1)
  {
#ifdef INVADER_USE_NUCLEO_LEDS
	powerToggle(NUCLEO_LED_3);
#endif
	HAL_Delay(1000);
  }
}


/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSI)
  *            SYSCLK(Hz)                     = 200000000
  *            HCLK(Hz)                       = 200000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 400
  *            PLL_P                          = 2
  *            PLL_Q                          = 9
  *            PLL_R                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
void PDU_SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  HAL_StatusTypeDef ret = HAL_OK;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is
     clocked below the maximum system frequency, to update the voltage scaling value
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  // Use the 16MHz HSI Oscillator (on by default)
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  RCC_OscInitStruct.PLL.PLLR = 7;

  ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
  if(ret != HAL_OK)
  {
    Error_Handler();
  }

  /* Activate the OverDrive */
  ret = HAL_PWREx_EnableOverDrive();
  if(ret != HAL_OK)
  {
    Error_Handler();
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);
  if(ret != HAL_OK)
  {
    Error_Handler();
  }
}



