/*
 * Simple microseconds delay routine, utilizing ARM's DWT
 * (Data Watchpoint and Trace Unit) and HAL library.
 * Intended to use with gcc compiler, but I hope it can be used
 * with any other C compiler across the Universe (provided that
 * ARM and CMSIS already invented) :)
 * Max K
 *
 *
 * This file is part of DWT_Delay package.
 * DWT_Delay is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * us_delay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details.
 * http://www.gnu.org/licenses/.
 */

#include "stm32f7xx_hal.h"          // change to whatever MCU you use
#include "dwt_delay.h"

/***************************************************
 ************Initialization routine*****************
 *
 * -Enables and configures the DWT (Data Watchpoint
 * 	and Trace Unit)
 *
 * 	 http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0489b/BIIFBHIF.html
 *
 ***************************************************/
void DWT_Init(void)
{
	// This sets the TRCENA bit to enable the "trace and debug blocks"
	// http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0337e/CEGHJDCF.html
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

    // Can't find much about why this is needed, but it makes things work
	#if __CORTEX_M == 7
		// Unlock DWT.
		DWT->LAR = 0xC5ACCE55; // Enables user access to the DWT
	#endif

	// Reset the DWT cycle counter to 0
    DWT->CYCCNT = 0;
    // Enable/start the DWT cycle counter
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;

    // Ready to be used for usDelay()
}

/****************************************************************
 * Delay routine itself.
 * Time is in microseconds (1/1000000th of a second), not to be
 * confused with millisecond (1/1000th).
 *
 * No need to check an overflow. Let it just tick :)
 *
 * @param uint32_t us  Number of microseconds to delay for
 ****************************************************************/
void DWT_Delay(uint32_t us) // microseconds
{

	// Get the current cycle count from the DWT
    uint32_t startTick = DWT->CYCCNT;

    // Calculate how many counts to wait based on system core clock frequency
    uint32_t delayTicks = us * (SystemCoreClock/1000000);

    // Loop until the delay time has passed
    while (DWT->CYCCNT - startTick < delayTicks);
}
