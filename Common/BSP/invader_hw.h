///
/// Hardware defines common to all hardware variants.
///
/// Generally, _don't_ include this file, include invader_<variant>_hw.h
/// instead, which will include this file.
///
  
#ifndef __BSP_INVADER_HW__
#define __BSP_INVADER_HW__

/// \addtogroup Common
/// @{
///
/// \addtogroup BSP
/// @{

// Pull project-specific configuration (which board are we targeting?)
// from invader_hw_conf.
#include "invader_hw_conf.h"

#ifdef DEBUG
#define BUILD_TYPE "Debug"
#else
#define BUILD_TYPE "Release"
#endif

//=== Include board-specific configuration


#ifdef INVADER_BOARD_PFU

	#define INVADER_USE_ETHERNET

	#include "invader_hw_pfu.h"

#elif defined INVADER_BOARD_PDU

	#define INVADER_UARTx_AS_SIC

	#include "invader_hw_pdu.h"

#elif INVADER_BOARD_NUCLEO

	#define INVADER_USE_ETHERNET
	#define INVADER_VCP_AS_SIC

#else
	#error "INVADER_BOARD_* must be defined"
#endif


/* Includes ------------------------------------------------------------------*/
#include "system_stm32f7xx.h"
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_gpio.h"
#include "stdbool.h"


//==== SIC Serial link configuration ===

#define SIC_USARTx_BAUD						115200

#ifdef INVADER_UARTx_AS_SIC

//======== Use the real hardware Com port for the SIC ===
#define SIC_USARTx                           UART7
#define SIC_USARTx_CLK_ENABLE()              __HAL_RCC_UART7_CLK_ENABLE()
#define SIC_USARTx_GPIO_CLK_ENABLE()         __HAL_RCC_GPIOE_CLK_ENABLE()

#define SIC_USARTx_FORCE_RESET()             __HAL_RCC_UART7_FORCE_RESET()
#define SIC_USARTx_RELEASE_RESET()           __HAL_RCC_UART7_RELEASE_RESET()

/* Definition for USARTx Pins */
#define SIC_USARTx_TX_PIN                    GPIO_PIN_8
#define SIC_USARTx_RX_PIN                    GPIO_PIN_7
#define SIC_USARTx_GPIO_PORT                 GPIOE
#define SIC_USARTx_AF                        GPIO_AF8_UART7

/* Definition for USARTx's NVIC IRQ and IRQ Handlers */
#define SIC_USARTx_IRQn                      UART7_IRQn
#define SIC_USARTx_IRQHandler                UART7_IRQHandler

#elif INVADER_VCP_AS_SIC

//===   Use the Virtual Com Port provided by STLink as the SIC serial port
#define SIC_USARTx                           USART3
#define SIC_USARTx_CLK_ENABLE()              __HAL_RCC_USART3_CLK_ENABLE()
#define SIC_USARTx_GPIO_CLK_ENABLE()         __HAL_RCC_GPIOD_CLK_ENABLE()

#define SIC_USARTx_FORCE_RESET()             __HAL_RCC_USART3_FORCE_RESET()
#define SIC_USARTx_RELEASE_RESET()           __HAL_RCC_USART3_RELEASE_RESET()

/* Definition for USARTx Pins */
#define SIC_USARTx_TX_PIN                    GPIO_PIN_8
#define SIC_USARTx_RX_PIN                    GPIO_PIN_9
#define SIC_USARTx_GPIO_PORT                 GPIOD
#define SIC_USARTx_AF                        GPIO_AF7_USART3

///* Definition for USARTx's NVIC IRQ and IRQ Handlers */
#define SIC_USARTx_IRQn                      USART3_IRQn
#define SIC_USARTx_IRQHandler                USART3_IRQHandler

#endif

#define SIC_NVIC_PRIORITY					 6


enum SpiSpecial {
	SPI_NOT_SPECIAL,
	SPI_ON_LASER_MUX,
	SPI_NEEDS_3TO5_TRISTATE
};

struct SPIBusChipSelect {
	enum SpiBusChipSelectList csIdx;
	GPIO_TypeDef* GPIOx;
	uint16_t pin;
	uint8_t  spiMode;
#ifdef INVADER_BOARD_PDU
	enum SpiSpecial	 isSpecial;
	uint8_t  laserMux;
#endif
};

extern const struct SPIBusChipSelect _spiChipSelects[];
extern const int numSpiChipSelects;


#include "Drivers/adc_LTC1867/adc_LTC1867_types.h"


//===================================================================
//        System SPI bus
//===================================================================

/* User can use this section to tailor SPIx instance used and associated
   resources */
/* Definition for SPI clock resources */
#define SYS_SPI                             SPI4
#define SYS_SPI_CLK_ENABLE()                __HAL_RCC_SPI4_CLK_ENABLE()
#define SYS_SPI_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOE_CLK_ENABLE()

#define SYS_SPI_FORCE_RESET()               __HAL_RCC_SPI4_FORCE_RESET()
#define SYS_SPI_RELEASE_RESET()             __HAL_RCC_SPI4_RELEASE_RESET()

/* Definition for SPI Pins */
// (all three pins on the same GPIO port)
#define SYS_SPI_SCK_PIN                     GPIO_PIN_12
#define SYS_SPI_MISO_PIN                    GPIO_PIN_13
#define SYS_SPI_MOSI_PIN                    GPIO_PIN_14
#define SYS_SPI_GPIO_PORT                   GPIOE
#define SYS_SPI_AF                          GPIO_AF5_SPI4

#define SPITimeout							100

//=== Devices on the System SPI bus ===

//===================================================================
//        System I2C bus
//===================================================================

//#include "hw_i2c_sys.h"

#define SYS_I2C								I2C1
#define SYS_I2Cx_CLK_ENABLE()               __HAL_RCC_I2C1_CLK_ENABLE()
#define SYS_I2Cx_SCL_SDA_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOB_CLK_ENABLE()

#define SYS_I2Cx_FORCE_RESET()              __HAL_RCC_I2C1_FORCE_RESET()
#define SYS_I2Cx_RELEASE_RESET()            __HAL_RCC_I2C1_RELEASE_RESET()

/* Definition for I2C1 Pins */
#define SYS_I2Cx_SCL_PIN                    GPIO_PIN_8
#define SYS_I2Cx_SDA_PIN                    GPIO_PIN_9
#define SYS_I2Cx_SCL_SDA_GPIO_PORT          GPIOB
#define SYS_I2Cx_SCL_SDA_AF                 GPIO_AF4_I2C1

#define SYS_I2C_TIMING        	0x00303D5B   //0x00D00E28  /* (Rise time = 120ns, Fall time = 25ns) */
#define SYS_I2C_TIMEOUT			100

//-- MCP4725 addressing
#define dacMCPWriteData 					Sys_I2C_WriteData
#define dacMCPReadData						Sys_I2C_ReadData

// Technically, the MCP4725 stores the address in the 7 highest bits of
// the address byte.   For simplicity, store the address shifted up
#define FOCUS_DAC_ADDRESS ((0x60)<<1)

//===================================================================
//        Power switching GPIOs
//===================================================================

struct PowerEnableEntry {
	uint8_t psIdx;
	const char *name;
	GPIO_TypeDef* GPIOx;
	uint16_t pin;
	bool stateOnReset;
	void (*beforePowerOn)(void);
	void (*afterPowerOn)(void);
	void (*beforePowerOff)(void);
	void (*afterPowerOff)(void);
};



extern struct PowerEnableEntry powerEnables[];
extern const int numPowerEnables;


/// \brief Wait for specified microseconds.
///
/// The accuracy of the timing on this function is not specified.   This function
/// is almost certainly just an idle loop....
extern void usDelay( uint32_t us );


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line);
#endif



#ifdef INVADER_USE_ETHERNET

#define INVADER_SIC_PORT (5555)

#endif




/// @}
/// @}


#endif
