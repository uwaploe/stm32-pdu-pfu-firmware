/**
  ******************************************************************************
  * @file    UART/UART_HyperTerminal_IT/Src/stm32f7xx_hal_msp.c
  * @author  MCD Application Team
  * @brief   HAL MSP module.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2016 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */



/* Includes ------------------------------------------------------------------*/
#include "invader_hw.h"

#include "hw_uart_sic.h"
#include "hw_spi_sys.h"
#include "hw_i2c_sys.h"

#ifdef INVADER_BOARD_PDU
#include "Drivers/hcb_uart/hcb_uart.h"
#endif


//!! Remember, these functions are called from within HAL_UART_Init, etc. which
//   is called from within user code.   But it happens inside some hardware
//   locking and etc.  So think of it as a "now it's time to configure the hardware"
//   callback.


#if defined INVADER_UARTx_AS_SIC || defined INVADER_VCP_AS_SIC
///
/// HAL callback on UART initialization.
///
/// Called from deep within HAL_UART_Init().
///        This function configures the hardware resources used in this example:
///           - Peripheral's clock enable
///           - Peripheral's GPIO Configuration
///           - Peripheral's GPIO Configuration
///           - NVIC configuration for UART interrupt request enable
///
/// \param huart: UART handle pointer
/// \retval None
///
void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
	if( huart->Instance == SIC_USARTx ) {
		SicHALInit();
	}
#ifdef INVADER_BOARD_PDU
	else if( huart->Instance == HCB_USARTx ) {
		HcbHALInit();
	}
#endif
}

///
/// HAL callback on UART de-initialization.
///
/// Called from deep within HAL_UART_DeInit()...
///
/// \param huart: UART handle pointer
/// \retval None
///
void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{
	if( huart->Instance == SIC_USARTx ) {
		SicHALDeInit();
	}
#ifdef INVADER_BOARD_PDU
	else if( huart->Instance == HCB_USARTx ) {
		HcbHALDeInit();
	}
#endif
}
#endif

///
/// HAL callback on SPI initialization.
///
/// Called from deep within HAL_SPI_Init().
///        This function configures the hardware resources used in this example:
///           - Peripheral's clock enable
///           - Peripheral's GPIO Configuration
///           - Peripheral's GPIO Configuration
///           - NVIC configuration for UART interrupt request enable
///
/// \param hspi: SPI handle pointer
/// \retval None
///
void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
  if(hspi->Instance == SYS_SPI) { Sys_SPI_HAL_Init(); }
}


///
/// HAL callback on SPI de-initialization.
///
/// Called from deep within HAL_SPI_DeInit()...
///
/// \param hspi: SPi handle pointer
/// \retval None
///
void HAL_SPI_MspDeInit(SPI_HandleTypeDef *hspi)
{
   if(hspi->Instance == SYS_SPI) { Sys_SPI_HAL_DeInit(); }
}

///
/// HAL callback on I2C de-initialization.
///
/// Called from deep within HAL_I2C_Init()...
///
/// \param hi2c: SPi handle pointer
/// \retval None
///
void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c)
{
  if(hi2c->Instance == SYS_I2C) { Sys_I2C_HAL_Init(); }
}

///
/// HAL callback on I2C de-initialization.
///
/// Called from deep within HAL_I2C_DeInit()...
///
/// \param hi2ci: SPi handle pointer
/// \retval None
///
void HAL_I2C_MspDeInit(I2C_HandleTypeDef *hi2c)
{
  if(hi2c->Instance == SYS_I2C) { Sys_I2C_HAL_DeInit(); }
}
