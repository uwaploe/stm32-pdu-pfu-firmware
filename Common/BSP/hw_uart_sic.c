


#include "BSP/invader_hw.h"

#include <stdint.h>
#include <stdbool.h>

#include "stm32f7xx_hal.h"
#include "hw_uart_sic.h"

// Right now, this breaks the abstraction
#include "Drivers/sic_io.h"


UART_HandleTypeDef SICUartHandle;

#define SIC_NOT_TRANSMITTING 255
uint8_t SicCurrentBuffer = SIC_NOT_TRANSMITTING;

/// ISR provided by the HAL writes to this buffer.  It then triggers
/// HAL_UART_RxCpltCallback when a character has been received
///
uint8_t _rxTempBuffer[2];

/// Abstract function to support sic_io
///
/// Essentially "the TxBuffer idx is ready to send"
///
void SicBufferPublish( SicBufferId_t idx ) {
	if( SICUartHandle.gState == HAL_UART_STATE_READY ) {
		SicCurrentBuffer = idx;
		HAL_StatusTypeDef retval = HAL_UART_Transmit_IT( &SICUartHandle, (uint8_t *)_sicTxBuffers[idx].buf, _sicTxBuffers[idx].length );
	}
}


///
/// Called by HAL when the transmission has completed
///
void SicTxComplete(UART_HandleTypeDef *huart) {

  SicBufferRelease( SicCurrentBuffer );

  // Check if another buffer is ready to go
  SicBufferId_t next = SicNextReadyBuffer();

  if( next >= 0 ) {
	  SicCurrentBuffer = next;
	  HAL_StatusTypeDef retval = HAL_UART_Transmit_IT( &SICUartHandle, (uint8_t *)_sicTxBuffers[next].buf, _sicTxBuffers[next].length );
	  return;
  }

  SicCurrentBuffer = SIC_NOT_TRANSMITTING;
}

///
/// Called by HAL when a new character has been received
///
void SicRxComplete(UART_HandleTypeDef *huart) {
	// Process incoming message

	const uint8_t inchar = _rxTempBuffer[0];

	if( inchar == '\r' || inchar == '\n' ) {

		_sicRxBuffer.buf[ _sicRxBuffer.length ] = '\0';

		// Post message to queue
		osMessagePut( InputMsgBox, 0, 0 );
	}

	if( inchar == '$' ) {
		// If you get a start-of-message character, reset the buffer
		_sicRxBuffer.length = 0;
	}

	_sicRxBuffer.buf[ _sicRxBuffer.length++ ] = _rxTempBuffer[0];

	if( _sicRxBuffer.length >= BUFFER_LEN ) {
		// If it's overlength, just give up?
		_sicRxBuffer.length = 0;
		goto reset;
	}

reset:
	HAL_UART_Receive_IT(&SICUartHandle, _rxTempBuffer, 1 );

}



//========================================================================
//
//
//========================================================================


void SicHALInit( void )
{
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  SIC_USARTx_GPIO_CLK_ENABLE();
  SIC_USARTx_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* UART TX GPIO pin configuration  */
  GPIO_InitTypeDef  GPIO_InitStruct;

  GPIO_InitStruct.Pin       = SIC_USARTx_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = SIC_USARTx_AF;

  HAL_GPIO_Init(SIC_USARTx_GPIO_PORT, &GPIO_InitStruct);

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = SIC_USARTx_RX_PIN;

  HAL_GPIO_Init(SIC_USARTx_GPIO_PORT, &GPIO_InitStruct);

  /*##-3- Configure the NVIC for UART ########################################*/
  /* NVIC for USARTx */
  HAL_NVIC_SetPriority(SIC_USARTx_IRQn, SIC_NVIC_PRIORITY, 0 );
  HAL_NVIC_EnableIRQ(SIC_USARTx_IRQn);
}

void SicHALDeInit(void)
{
	/*##-1- Reset peripherals ##################################################*/
	SIC_USARTx_FORCE_RESET();
	SIC_USARTx_RELEASE_RESET();

  /*##-2- Disable peripherals and GPIO Clocks ################################*/
  /* Configure UART Tx as alternate function  */
  HAL_GPIO_DeInit(SIC_USARTx_GPIO_PORT, SIC_USARTx_TX_PIN);
  /* Configure UART Rx as alternate function  */
  HAL_GPIO_DeInit(SIC_USARTx_GPIO_PORT, SIC_USARTx_RX_PIN);

  /*##-3- Disable the NVIC for UART ##########################################*/
  HAL_NVIC_DisableIRQ(SIC_USARTx_IRQn);
}


/**
  * @brief  Function called in case of error detected in USART IT Handler
  * @param  None
  * @retval None
  */
static void SIC_Error_Callback(void)
{
  __IO uint32_t isr_reg;

  /* Disable USARTx_IRQn */
  NVIC_DisableIRQ(SIC_USARTx_IRQn);

  /* Error handling example :
    - Read USART ISR register to identify flag that leads to IT raising
    - Perform corresponding error handling treatment according to flag
  */
//  isr_reg = LL_USART_ReadReg(SIC_USARTx, ISR);

//  if (isr_reg & LL_USART_ISR_NE)
//  {
//    /* case Noise Error flag is raised : ... */
//    //LED_Blinking(LED_BLINK_FAST);
//  }
//  else
//  {
//    /* Unexpected IT source : Set LED to Blinking mode to indicate error occurs */
//    //LED_Blinking(LED_BLINK_ERROR);
//  }
}

/**
  * @brief  This function configures USARTx Instance.
  * @note   This function is used to :
  *         -1- Enable GPIO clock and configures the USART pins.
  *         -2- NVIC Configuration for USART interrupts.
  *         -3- Enable the USART peripheral clock and clock source.
  *         -4- Configure USART functional parameters.
  *         -5- Enable USART.
  * @note   Peripheral configuration is minimal configuration from reset values.
  *         Thus, some useless LL unitary functions calls below are provided as
  *         commented examples - setting is default configuration from reset.
  * @param  None
  * @retval None
  */
void SicUARTInit(void)
{
	/*##-1- Configure the UART peripheral using HAL services ###################*/
	/* Put the USART peripheral in the Asynchronous mode (UART Mode) */
	  /* UART configured as follows:
	      - Word Length = 8 Bits (7 data bit + 1 parity bit) :
	                          BE CAREFUL : This is equivalent to 7 data bits + 1 parity bit in conventional nomenclature
	      - Stop Bit    = One Stop bit
	      - Parity      = ODD parity
	      - BaudRate    = 9600 baud
	      - Hardware flow control disabled (RTS and CTS signals) */
	SICUartHandle.Instance        = SIC_USARTx;

	SICUartHandle.Init.BaudRate   = SIC_USARTx_BAUD;
	SICUartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	SICUartHandle.Init.StopBits   = UART_STOPBITS_2;
	SICUartHandle.Init.Parity     = UART_PARITY_NONE;
	SICUartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	SICUartHandle.Init.Mode       = UART_MODE_TX_RX;

	if(HAL_UART_Init(&SICUartHandle) != HAL_OK)
	{
	  /* Initialization Error */
		SIC_Error_Callback();
	}

	// Kick off receive cycle
	HAL_UART_Receive_IT(&SICUartHandle, _rxTempBuffer, 1 );

}




