///
/// Hardware abstraction for power control switches
///

#ifndef __INVADER_BSP_HW_POWER_ENABLE_H__
#define __INVADER_BSP_HW_POWER_ENABLE_H__

#include <stdbool.h>
#include "invader_hw.h"

bool initPowerEnable( void );

void powerOn( enum PowerEnable idx );
void powerOff( enum PowerEnable idx );

void powerToggle( enum PowerEnable idx );

bool powerState( enum PowerEnable idx );

enum PowerEnable findPowerEnable( const char *str );
const char *powerEnableName( enum PowerEnable idx );

#endif
