/*
 * hw_uart_SIC.h
 *
 *  Created on: Sep 17, 2019
 *      Author: aaron
 */

#ifndef __BSP_HW_SPI_SYS_H__
#define __BSP_HW_SPI_SYS_H__

#include <stdbool.h>

#include "invader_hw.h"

/// \addtogroup Common
/// @{
///
/// \addtogroup BSP
/// @{

void Sys_SPI_HAL_Init(void);
void Sys_SPI_HAL_DeInit(void);

void Sys_SPI_Configure(void);


// These are the "rawest" functions, performing read and write on data
bool    Sys_SPI_WriteReadData( enum SpiBusChipSelectList idx, uint8_t *dataOut, uint8_t *dataIn, uint16_t DataLength);
bool    Sys_SPI_WriteData(     enum SpiBusChipSelectList idx, uint8_t *dataOut, uint16_t DataLength);

bool    LTC1867_SPI_WriteReadData( enum SpiBusChipSelectList idx, uint8_t *dataOut, uint8_t *dataIn, uint16_t DataLength);

//void    Sys_SPI_ReadData(     uint8_t idx, uint8_t *dataOut, uint16_t DataLength);
//void    Sys_SPI_Write(        uint8_t idx, uint8_t Value);
//uint8_t Sys_SPI_Read(         uint8_t idx );


// These functions add an additional level of understanding SPI register addressing
void    Sys_SPI_WriteAddr(    enum SpiBusChipSelectList idx, uint8_t addr, uint8_t data );
void    Sys_SPI_WriteDataAddr( enum SpiBusChipSelectList idx, uint8_t addr, uint8_t *data, uint16_t length );

uint8_t Sys_SPI_ReadAddr(     enum SpiBusChipSelectList idx, uint8_t addr );
uint8_t Sys_SPI_ReadDataAddr( enum SpiBusChipSelectList idx, uint8_t addr, uint8_t *data, uint16_t length );

void Sys_SPI_Error(void);

/// @} @}

#endif
