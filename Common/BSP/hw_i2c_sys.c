///
/// hw_i2c_sys.h
///
/// \author Aaron Marburg <amarburg@uw.edu>
///

#include "BSP/invader_hw.h"

#include "stm32f7xx_hal_spi.h"



I2C_HandleTypeDef I2cHandle;



/**
  * @brief  Initializes SPI MSP.
  * @param  None
  * @retval None
  */
void Sys_I2C_HAL_Init()
{
  GPIO_InitTypeDef  GPIO_InitStruct;

  /* Enable GPIO and peripheral clocks */
  SYS_I2Cx_SCL_SDA_GPIO_CLK_ENABLE();

  /* I2C SCL GPIO pin configuration  */
  GPIO_InitStruct.Pin       = SYS_I2Cx_SCL_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = SYS_I2Cx_SCL_SDA_AF;
  HAL_GPIO_Init(SYS_I2Cx_SCL_SDA_GPIO_PORT, &GPIO_InitStruct);

  /* I2C SDA GPIO pin configuration  */
  GPIO_InitStruct.Pin       = SYS_I2Cx_SDA_PIN;
  HAL_GPIO_Init(SYS_I2Cx_SCL_SDA_GPIO_PORT, &GPIO_InitStruct);

  /*##-3- Configure the NVIC for I2C ########################################*/
  /* NVIC for I2Cx */
//  HAL_NVIC_SetPriority(I2Cx_ER_IRQn, 0, 1);
//  HAL_NVIC_EnableIRQ(I2Cx_ER_IRQn);
//  HAL_NVIC_SetPriority(I2Cx_EV_IRQn, 0, 2);
//  HAL_NVIC_EnableIRQ(I2Cx_EV_IRQn);

  SYS_I2Cx_CLK_ENABLE();
}

void Sys_I2C_HAL_DeInit(void)
{
  /*##-1- Reset peripherals ##################################################*/
  SYS_I2Cx_FORCE_RESET();
  SYS_I2Cx_RELEASE_RESET();

  /*##-2- Disable peripherals and GPIO Clocks #################################*/
  /* Configure I2C Tx as alternate function  */
  HAL_GPIO_DeInit(SYS_I2Cx_SCL_SDA_GPIO_PORT, SYS_I2Cx_SCL_PIN);
  HAL_GPIO_DeInit(SYS_I2Cx_SCL_SDA_GPIO_PORT, SYS_I2Cx_SDA_PIN);

  /*##-3- Disable the NVIC for I2C ##########################################*/
//  HAL_NVIC_DisableIRQ(I2Cx_ER_IRQn);
//  HAL_NVIC_DisableIRQ(I2Cx_EV_IRQn);
}



/**
  * @brief  This function is executed in case of error occurrence.
  *
  * \todo This behavior is not right...
  * @param  None
  * @retval None
  */
static void Sys_I2C_Error(void)
{
  /* Turn LED3 on */
#ifdef INVADER_USE_NUCLEO_LEDS
  powerOn(NUCLEO_LED_3);
#endif
 // while(1) {;}
}


void Sys_I2C_Configure(void)
{
  /*##-1- Configure the SPI peripheral #######################################*/
  I2cHandle.Instance             = SYS_I2C;
  I2cHandle.Init.Timing          = SYS_I2C_TIMING;
  I2cHandle.Init.OwnAddress1     = 0x60;
  I2cHandle.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
  I2cHandle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  I2cHandle.Init.OwnAddress2     = 0xFF;
  I2cHandle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  I2cHandle.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;

  if(HAL_I2C_Init(&I2cHandle) != HAL_OK)
  {
	/* Initialization Error */
	  Sys_I2C_Error();

  }
}

//Polling HAL functions...
//HAL_StatusTypeDef HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout);
//HAL_StatusTypeDef HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout);
//HAL_StatusTypeDef HAL_I2C_Slave_Transmit(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size, uint32_t Timeout);
//HAL_StatusTypeDef HAL_I2C_Slave_Receive(I2C_HandleTypeDef *hi2c, uint8_t *pData, uint16_t Size, uint32_t Timeout);
//HAL_StatusTypeDef HAL_I2C_Mem_Write(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);
//HAL_StatusTypeDef HAL_I2C_Mem_Read(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);
//HAL_StatusTypeDef HAL_I2C_IsDeviceReady(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint32_t Trials, uint32_t Timeout);



void Sys_I2C_WriteData(uint16_t devAddress, uint8_t *outData, uint16_t length)
{
  HAL_StatusTypeDef status = HAL_OK;

  status = HAL_I2C_Master_Transmit(&I2cHandle, devAddress, outData, length, SYS_I2C_TIMEOUT);

  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    Sys_I2C_Error();
  }
}

void Sys_I2C_ReadData(uint16_t devAddress, uint8_t *inData, uint16_t length)
{
  HAL_StatusTypeDef status = HAL_OK;

  status = HAL_I2C_Master_Receive(&I2cHandle, devAddress, inData, length, SYS_I2C_TIMEOUT);

  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    Sys_I2C_Error();
  }
}

