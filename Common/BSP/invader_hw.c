/// Functions common to all hardware variants of PDU/PFU board

#include "invader_hw.h"
#include "hw_spi_sys.h"
#include "BSP/dwt_delay.h"


bool adcValidateIdx( uint8_t idx ) {
	if( idx >= numLtcAdcs ) return false;
	return true;
}

// ADCs get their own SPI  version ... all ADC transfers are 16 bits of read and write
bool adcSpiTransfer( uint8_t which, uint8_t *dataOut, uint8_t *dataIn ) {
	return LTC1867_SPI_WriteReadData( ltcAdcs[which].csIdx, dataOut, dataIn, 2 );
}


#define MAKE_SPI_IO_FUNCTIONS( prefix, ChipSelect ) \
	\
	 uint8_t prefix##ReadByte( uint8_t addr ) \
		{ return Sys_SPI_ReadAddr( ChipSelect, addr); } \
	\
	uint8_t prefix##ReadBytes( uint8_t addr, uint8_t *data, uint16_t length ) \
		{ return Sys_SPI_ReadDataAddr( ChipSelect, addr, data, length); } \
	\
	void    prefix##WriteByte( uint8_t addr, uint8_t data ) \
		{ Sys_SPI_WriteAddr( ChipSelect, addr, data  ); }

MAKE_SPI_IO_FUNCTIONS( imuXg, SPI_IMU_AG )
MAKE_SPI_IO_FUNCTIONS( imuM,  SPI_IMU_M )
MAKE_SPI_IO_FUNCTIONS( bme,   SPI_BME280 )


/*
 *
 *	Delay microseconds:
 *		This function calls the DWT_Delay() function which is just a while
 *		loop that keeps checking whether or not us*SystemClockFrequency/1000000 system
 *		ticks have occurred by checking the DWT cycle counter.
 *
 */
extern void usDelay( uint32_t us ) {
	//for( int i = 1000 * us; i>0; i--) {;}

	DWT_Delay(us); //delay us microseconds
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

