//======
//   Callbacks from the HAL
//
//   Remember it goes:
//
//      EXTI15_10_IRQHandler      ( in stm32f76xx_isr.c )
//  --> HAL_GPIO_EXTI_IRQHandler  ( in the HAL )
//  --> HAL_GPIO_EXTI_Callback
//
//======

#include "BSP/invader_hw.h"

#include "BSP/hw_uart_sic.h"

/// There's a bad abstraction breakage here...
#ifdef INVADER_BOARD_PDU
#include "Drivers/focus_motor/focus_motor.h"
#include "Drivers/hcb_uart/hcb_uart.h"
#endif


/// The "real" handler for The ISR calls HAL_GPIO_EXTI_IRQHandler(), which does some
/// housekeeping, then calls this function.
///
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

	if( GPIO_Pin == FOCUS_ENCODER_A_PIN ) {

		focusEncoderIRQ_Callback( ENCODER_A, HAL_GPIO_ReadPin( FOCUS_GPIO_PORT, FOCUS_ENCODER_A_PIN ) );

	} else if( GPIO_Pin == FOCUS_ENCODER_B_PIN ) {

		focusEncoderIRQ_Callback( ENCODER_B, HAL_GPIO_ReadPin( FOCUS_GPIO_PORT, FOCUS_ENCODER_B_PIN ) );

	}

}


void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
	if( huart == &SICUartHandle ) {
		SicTxComplete( huart );
	}
#ifdef INVADER_BOARD_PDU
	else if( huart == &HCBUartHandle ) {
		HcbTxComplete( huart );
	}
#endif
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	if( huart == &SICUartHandle ) {
		SicRxComplete( huart );
	}
#ifdef INVADER_BOARD_PDU
	else if( huart == &HCBUartHandle ) {
		HcbRxComplete( huart );
	}
#endif
}



//void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart);
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
//void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart);

