///
/// Hardware defines common to all hardware variants.
///
/// Generally, _don't_ include this file, include invader_<variant>_hw.h
/// instead, which will include this file.
///
  
#ifndef __BSP_INVADER_HW_PFU_H__
#define __BSP_INVADER_HW_PFU_H__

/// \addtogroup Common
/// @{
///
/// \addtogroup BSP
/// @{

#ifndef INVADER_BOARD_PFU
#error "invader_hw_pfu.h included when INVADER_BOARD_PFU not defined.  Error."
#endif

//=== Devices on the System SPI bus ===

enum SpiBusChipSelectList {
	SPI_IMU_AG,
	SPI_IMU_M,
	SPI_BME280,
	SPI_ADC0,
	NUM_SPI_CHIP_SELECT
};

//==== LTC1867 ADCs ===

#include "Drivers/adc_LTC1867/adc_LTC1867_types.h"

enum LtcAdcs {
	LTC_ADC0,
	NUM_LTC_ADCS
};



enum PowerEnable {
	NUM_POWER_ENABLES,
	POWER_ENABLE_NOT_FOUND
};


/// \brief System clock configuration specific to the PFU
void PFU_SystemClock_Config(void);
void PFU_MPU_Config(void);

/// @}
/// @}


#endif
