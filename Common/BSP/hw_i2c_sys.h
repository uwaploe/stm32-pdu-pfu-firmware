///
/// hw_i2c_sys.h
///
/// \author Aaron Marburg <amarburg@uw.edu>
///

#ifndef __BSP_HW_I2C_SYS_H__
#define __BSP_HW_I2C_SYS_H__

#include "invader_hw.h"

/// \addtogroup Common
/// @{
///
/// \addtogroup BSP
/// @{

void Sys_I2C_HAL_Init(void);
void Sys_I2C_HAL_DeInit(void);

void Sys_I2C_Configure(void);

void Sys_I2C_WriteData(uint16_t devAddress, uint8_t *outData, uint16_t length);
void Sys_I2C_ReadData(uint16_t devAddress, uint8_t *inData, uint16_t length);

/// @}
/// @}

#endif
