///
/// hw_uart_SIC.h
///
/// Implements the "hardware layer" for the UART-based console between a
/// control PC (usually the SIC) and this board.  The interface is engineered
/// for NMEA-style (fixed first character and line ending) input.
///
/// This file implements the function SicBufferPublish required by sic_io.h and
/// makes calls into sic_io

#ifndef __INVADER_BSP_HW_UART_SIC_H__
#define __INVADER_BSP_HW_UART_SIC_H__

#include <stdint.h>

#include "stm32f7xx_hal.h"
#include "cmsis_os.h"

/// \addtogroup Common
/// @{
///
/// \addtogroup BSP
/// @{
///
/// \addtogroup SicIO SIC Input/Output
/// @{

extern UART_HandleTypeDef SICUartHandle;

void SicHALInit(void);
void SicHALDeInit(void);

void SicUARTInit(void);

void SicTxComplete(UART_HandleTypeDef *huart);
void SicRxComplete(UART_HandleTypeDef *huart);

/// @} @} @}

#endif
