/**
  ******************************************************************************
  * @file    LwIP/LwIP_HTTP_Server_Netconn_RTOS/Src/stm32f7xx_it.c 
  * @author  MCD Application Team
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_isr.h"
#include "cmsis_os.h"

#include "BSP/invader_hw.h"

/// Breaks the abstraction for now...
#ifdef INVADER_BOARD_PDU
#include "BSP/hw_uart_sic.h"
#include "Drivers/hcb_uart/hcb_uart.h"
#endif


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M7 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  osSystickHandler();
}



/// \brief ISR for handling interrupts for USARTx used by SIC
///
/// Goes immediately to the HAL IRQ Handler (which then calls back out to
/// user functions)
///
/// (This name should be defined to one of the "magic" ISR function names like USART3_IRQHandler)
///

#if defined INVADER_UARTx_AS_SIC || defined INVADER_VCP_AS_SIC

#ifndef SIC_USARTx_IRQHandler
#error "SIC_USARTx_IRQHandler must be defined in stm32f7xx_isr.c"
#endif

void SIC_USARTx_IRQHandler( void ) {
#ifdef INVADER_UARTx_AS_SIC
	HAL_UART_IRQHandler( &SICUartHandle );
#endif
}

#endif

/// IRQ Handler for Heater control board USART
#ifdef INVADER_BOARD_PDU
void HCB_USARTx_IRQHandler( void ) {
	HAL_UART_IRQHandler( &HCBUartHandle );
}
#endif

/// \brief   ISR for handling interrupts from the EXTI15_10 block
///
/// Use the design pattern of passing off to the HAL IRQHandler.  This does
/// some housekeeping then calls HAL_GPIO_EXTI_Callback found in
/// invader_hal_callbacks.c
///
void EXTI15_10_IRQHandler(void)
{

    if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_15) ) {
    	HAL_GPIO_EXTI_IRQHandler( GPIO_PIN_15 );
    }

    if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_14) ) {
    	HAL_GPIO_EXTI_IRQHandler( GPIO_PIN_14 );
    }

}

/// \brief   ISR for handling interrupts from the EXTI9_5 block
///
/// Use the design pattern of passing off to the HAL IRQHandler.  This does
/// some housekeeping then calls HAL_GPIO_EXTI_Callback found in
/// invader_hal_callbacks.c
///
void EXTI9_5_IRQHandler(void)
{
    if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_9) ) {
    	HAL_GPIO_EXTI_IRQHandler( GPIO_PIN_9 );
    }

// Not using EXTI on these GPIOs yet, uncomment to enable
//    if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_8) ) {
//    	HAL_GPIO_EXTI_IRQHandler( GPIO_PIN_8 );
//    }
//
//    if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_7) ) {
//    	HAL_GPIO_EXTI_IRQHandler( GPIO_PIN_7 );
//    }
//
//    if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_6) ) {
//    	HAL_GPIO_EXTI_IRQHandler( GPIO_PIN_6 );
//    }
}


#ifdef INVADER_USE_ETHERNET

extern ETH_HandleTypeDef EthHandle;

void ETH_IRQHandler(void)
{
  HAL_ETH_IRQHandler(&EthHandle);
}

#endif


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
