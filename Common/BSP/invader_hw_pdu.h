///
/// Hardware defines common to all hardware variants.
///
/// Generally, _don't_ include this file, include invader_<variant>_hw.h
/// instead, which will include this file.
///
  
#ifndef __BSP_INVADER_HW_PDU_H__
#define __BSP_INVADER_HW_PDU_H__

/// \addtogroup Common
/// @{
///
/// \addtogroup BSP
/// @{

#ifndef INVADER_BOARD_PDU
#error "invader_hw_pdu.h included when INVADER_BOARD_PDU not defined.  This shouldn't happen!."
#endif


//=== Devices on the System SPI bus ===

enum SpiBusChipSelectList {
	SPI_IMU_AG,
	SPI_IMU_M,
	SPI_BME280,
	SPI_ADC0,
	SPI_ADC1,
	SPI_LASER_CS,
	SPI_LASER_SSA0,
	SPI_LASER_SSA1,
	SPI_LASER_ADC,
	SPI_LASER_DAC,
	SPI_PDUB_ADC1,
	SPI_PDUB_ADC2,
	SPI_PDUB_ADC3,
	SPI_PDUB_ADC4,
	SPI_CS_3TO5_TRISTATE,
	NUM_SPI_CHIP_SELECT

};


#include "Drivers/adc_LTC1867/adc_LTC1867_types.h"

enum LtcAdcs {
	LTC_ADC0,
	LTC_ADC1,
	LTC_ADC2,
	LTC_ADC3,
	LTC_ADC4,
	LTC_ADC5,
	LTC_ADC_LASER,
	NUM_LTC_ADCS
};

enum PowerEnable {
	TB_EN,
	UV_EN,
	BEXP_EN,
	CCD_EN,
	HCB_EN,
	HCB_MODE,
	CAM1_EN,
	CAM2_EN,
	LED1_EN,
	LED2_EN,
	EXPAN_EN,
	FOCUS_EN,
	LASER_ISO_5,
	LASER_ISO_28,
	LASER_ISO_12,
	NUM_POWER_ENABLES,
	POWER_ENABLE_NOT_FOUND
};

//===================================================================
//        Focus motor
//===================================================================

#define FOCUS_CONTROL_1_PIN    				GPIO_PIN_11
#define FOCUS_CONTROL_2_PIN					GPIO_PIN_12
#define FOCUS_CONTROL_PORT					GPIOA

#define FOCUS_CONTROL_CLK_ENABLE()          __HAL_RCC_GPIOA_CLK_ENABLE()


#define FOCUS_LIMIT_PIN                    GPIO_PIN_9
#define FOCUS_ENCODER_A_PIN                GPIO_PIN_14
#define FOCUS_ENCODER_B_PIN				   GPIO_PIN_15
#define FOCUS_GPIO_PORT                    GPIOD

#define FOCUS_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOD_CLK_ENABLE()

// Input interrupts
#define FOCUS_ENCODER_IRQ				EXTI15_10_IRQn
#define FOCUS_LIMIT_IRQ					EXTI9_5_IRQn


//===================================================================
//        UART for HCB comms
//===================================================================

//======== Use the real hardware Com port for the SIC ===
#define HCB_USARTx_BAUD						57600

#define HCB_USARTx                           UART8
#define HCB_USARTx_CLK_ENABLE()              __HAL_RCC_UART8_CLK_ENABLE()
#define HCB_USARTx_GPIO_CLK_ENABLE()         __HAL_RCC_GPIOE_CLK_ENABLE()

#define HCB_USARTx_FORCE_RESET()             __HAL_RCC_UART8_FORCE_RESET()
#define HCB_USARTx_RELEASE_RESET()           __HAL_RCC_UART8_RELEASE_RESET()

/* Definition for USARTx Pins */
#define HCB_USARTx_TX_PIN                    GPIO_PIN_1
#define HCB_USARTx_RX_PIN                    GPIO_PIN_0
#define HCB_USARTx_GPIO_PORT                 GPIOE
#define HCB_USARTx_AF                        GPIO_AF8_UART8

/* Definition for USARTx's NVIC IRQ and IRQ Handlers */
#define HCB_USARTx_IRQn                      UART8_IRQn
#define HCB_USARTx_IRQHandler                UART8_IRQHandler

#define HCB_NVIC_PRIORITY					 6


/// \brief System clock configuration specific to the PDU
void PDU_SystemClock_Config(void);

/// @}
/// @}


#endif
