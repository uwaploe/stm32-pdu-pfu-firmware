/*
 * hw_nucleo_leds.h
 *
 *  Created on: Sep 17, 2019
 *      Author: aaron
 */

#ifndef __BSP_HW_NUCLEO_LEDS_H__
#define __BSP_HW_NUCLEO_LEDS_H__

/// \addtogroup Common
/// @{
///
/// \addtogroup BSP
/// @{

#define LEDn 3

typedef enum
{
  LED1 = 0,
  LED_GREEN = LED1,
  LED2 = 1,
  LED_BLUE = LED2,
  LED3 = 2,
  LED_RED = LED3
} Led_TypeDef;


void             BSP_LED_Init(Led_TypeDef Led);
void             BSP_LED_DeInit(Led_TypeDef Led);
void             BSP_LED_On(Led_TypeDef Led);
void             BSP_LED_Off(Led_TypeDef Led);
void             BSP_LED_Toggle(Led_TypeDef Led);

/// @}
/// @}

#endif
