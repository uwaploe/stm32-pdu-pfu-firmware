
This repo contains the firmware for the two STM32-based boards in the NASA InVADER instrument:

 * The Power Distribution Board (PDU) in the A2 pressure vessel (directory `PDU/`)
 * The Power Filter Board (PFU) in the A1 pressure vessel (directory `PFU/`)

These two boards have a high overlap in capabilities and share a significant fraction of the code.

# Organization

The `PDU/` and `PFU/` directories each contain a standalone [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) / Eclipse project for the respective boards.

Code shared between the PDU and PFU is stored in the `Common/` directory.  The projects each import the required code from `Common/` using virtual links in Eclipse (like symlinks).

More detail in [Software Structure](Documentation/SoftwareStructure.md)

## Documentation

Documentation is in the [Documentation/](Documentation/) subdirectory.

Doxygen documentation can be built in the `Documentation/` directory with "doxygen stm32-pdu-pfu-firmware.doxyfile"   The resulting documentation is built into [`Documentation/Doxygen/`](Documentation/Doxygen/html/index.html)

---

# Code status

Code requirements are tied back to the relevant pages in the InVADER RVM.

| Subsystem  | Rqmnt  | Status |
| --- | --- | -- |
| UART: SIC serial interface output | PDU20 | Working |
| UART: NMEA data output formatting | PDU33  Working |
| UART: SIC serial interface input | PDU20, PDU21 | Working |
| UART: NMEA data input parsing / response | PDU32 | Working |
| UART: Interface to heater control board | PDU39 | Code in place, needs to be tested against HCB hardware.  |
| Power switch control -- lasers | PDU21, PDU40 | Working |
| Power switch control -- everything else | PDU21, PDU15  | Working |
| I/V monitoring | PDU12 | Working, uncalibrated |
| Temperature monitoring | PDU37 | Working? |
| Ground fault monitoring | PDU37 | |
| Heater control board interface | PDU38 | As above, UART code in place, needs to be tested.  |
| RTOS:  Periodic tasks | PDU33, PDU34 | Working. |
| SPI: LSM9DS1 Accel/Gyro | PDU36 | Accelerometer and gyro I/O working.   Accel data looks good, **scaling issues with gyro?** |  
| SPI: LSM9DS1 Magnetometer | PDU36 | Mag not working yet |  
| SPI: BME280 pressure/humidity/temp sensor | PDU35 | Working. |
| SPI: LT1867 ADC  | PDU37, PDU45 | Working, issue with last channel of ADC3?. |
| SPI: ADC8562 DAC (target laser power)| Working, max value still needs to be defined. |
| GPIO: Power switches | PDU32 | Stubbed in, not tested |
| GPIO:  Quadrature decode | PDU17  | |
| GPIO:  Focus motor limit switch | PDU18 | IRQ working, focus motor control state machine not done. |
| I2C: MCP4725 DAC (focus speed control)| |  Zero-th order code working. |
| Science laser: pn/off | | Working |
| Science laser: Heartbeat signal | PDU25  | Implemented, requires testing  |
| Targetting laser: on/off | PDU40  | Working |
| Targetting laser: power control | PDU41 | Working |
| Targeting laser: I/V monitoring | PDU44 | Working |
| Targeting laser: heartbeat | PDU45 | Implemented, requires testing (same as science laser heartbeat) |
| Focus control: | PDU16 | **Only coarse open loop control** |


---

# Get started

Assuming [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) is installed, the simplest path to getting started is to install [Git](https://www.eclipse.org/egit/) packages into Eclipse.   Select `File -> Import` then `Git -> Projects from Git` in the pop-up window:

![](Documentation/images/eclipse_open_git_repository.png)

Then `Clone URI` (I think `Existing local repository` works too), clone this repo, then `Import existing Eclipse projects`.   This should create multiple Eclipse projects in the workspace:

 * `InVADER_PDU_Firmware` is the master firmware for the PDU.
 * `InVADER_PFU_FIrmware` is the master firmware for the PFU.


License
-------

FreeRTOS is distributed under the [MIT License](http://www.freertos.org/a00114.html).

LwIP is distributed under ["a BSD license"](http://www.nongnu.org/lwip/2_1_x/index.html).
