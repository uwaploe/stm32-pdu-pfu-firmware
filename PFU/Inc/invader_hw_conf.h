/*
 * invader_hw_conf.h
 *
 *  Created on: Sep 18, 2019
 *      Author: aaron
 */

#ifndef INVADER_HW_CONF_H_
#define INVADER_HW_CONF_H_


#define INVADER_BOARD_PFU


//#define USE_DHCP       /* enable DHCP, if disabled static address is used*/

/*Static IP ADDRESS*/
#define IP_ADDR0   10
#define IP_ADDR1   40
#define IP_ADDR2   7
#define IP_ADDR3   16

/*NETMASK*/
#define NETMASK_ADDR0   255
#define NETMASK_ADDR1   255
#define NETMASK_ADDR2   255
#define NETMASK_ADDR3   0

/*Gateway Address*/
#define GW_ADDR0   10
#define GW_ADDR1   40
#define GW_ADDR2   7
#define GW_ADDR3   1

#endif /* INVADER_HW_CONF_H_ */
