///**
//  ******************************************************************************
//  * @file    LwIP/LwIP_HTTP_Server_Netconn_RTOS/Src/httpser-netconn.c
//  * @author  MCD Application Team
//  * @brief   Basic http server implementation using LwIP netconn API
//  ******************************************************************************
//  * @attention
//  *
//  * <h2><center>&copy; Copyright (c) 2016 STMicroelectronics International N.V.
//  * All rights reserved.</center></h2>
//  *
//  * Redistribution and use in source and binary forms, with or without
//  * modification, are permitted, provided that the following conditions are met:
//  *
//  * 1. Redistribution of source code must retain the above copyright notice,
//  *    this list of conditions and the following disclaimer.
//  * 2. Redistributions in binary form must reproduce the above copyright notice,
//  *    this list of conditions and the following disclaimer in the documentation
//  *    and/or other materials provided with the distribution.
//  * 3. Neither the name of STMicroelectronics nor the names of other
//  *    contributors to this software may be used to endorse or promote products
//  *    derived from this software without specific written permission.
//  * 4. This software, including modifications and/or derivative works of this
//  *    software, must execute solely and exclusively on microcontroller or
//  *    microprocessor devices manufactured by or for STMicroelectronics.
//  * 5. Redistribution and use of this software other than as permitted under
//  *    this license is void and will automatically terminate your rights under
//  *    this license.
//  *
//  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
//  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
//  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
//  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
//  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
//  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//  *
//  ******************************************************************************
//  */
//
///* Includes ------------------------------------------------------------------*/
//#include "lwip/opt.h"
//#include "lwip/arch.h"
//#include "lwip/api.h"
//#include "lwip/apps/fs.h"
//#include "string.h"

#include "BSP/invader_hw.h"
#include "Drivers/sic_io.h"

#include "lwip/tcp.h"

#include "sic-socket-netconn.h"
#include "cmsis_os.h"


//#include <stdio.h>
//
///* Private typedef -----------------------------------------------------------*/
///* Private define ------------------------------------------------------------*/
//#define WEBSERVER_THREAD_PRIO    ( osPriorityAboveNormal )


struct tcp_pcb *sic_pcb;

void sic_socket_err(void *arg, err_t err) {

}

err_t sic_socket_sent(void *arg, struct tcp_pcb *tpcb, u16_t len)  {
	return ERR_OK;
}


err_t sic_socket_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err) {
	 tcp_recved( tpcb, 0 );

	 return ERR_OK;
}


//
void SicBufferPublish( SicBufferId_t idx ) {
	if( sic_pcb ) {
		uint32_t sz = _sicTxBuffers[idx].length;
		tcp_write( sic_pcb, (void *)&sz, 4, 0 );
		tcp_write( sic_pcb, _sicTxBuffers[idx].buf, _sicTxBuffers[idx].length, TCP_WRITE_FLAG_COPY );
		tcp_output( sic_pcb );

		// Can do immediately because we're making a copy
		SicBufferRelease( idx );
	}
}


err_t sic_socket_accept(void *arg, struct tcp_pcb *newpcb, err_t err) {


	//tcp_accepted()		//Server accepts connection, decrements "pending" session count
	//tcp_arg()			//(Server allocates new session structure), sets new callback argument
	tcp_recv( newpcb, sic_socket_recv );			//Server sets recv callback
	tcp_err( newpcb, sic_socket_err );			//Server sets error/abort callback
	tcp_sent( newpcb, sic_socket_sent );
	sic_pcb = newpcb;

	return ERR_OK;
}


/**
  * @brief  Initialize the HTTP server (start its thread)
  * @param  none
  * @retval None
  */
void sic_socket_netconn_init() {

	sic_pcb = tcp_new_ip_type(IPADDR_TYPE_V4);
	err_t err = tcp_bind( sic_pcb, IP_ANY_TYPE, INVADER_SIC_PORT );


	sic_pcb = tcp_listen( sic_pcb );

	tcp_accept( sic_pcb, sic_socket_accept );

//  sys_thread_new("HTTP", sic_socket_netconn_thread, NULL, DEFAULT_THREAD_STACKSIZE, WEBSERVER_THREAD_PRIO);
}
