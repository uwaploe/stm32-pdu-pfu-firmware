///
///
/// Main file for InVADER PDU firmware.
///

/// \addtogroup PDU_firmware
/// @{

/* Includes ------------------------------------------------------------------*/
#include "BSP/invader_hw.h"
#ifdef DEBUG
#include "BSP/invader_hw_validate.h"
#endif

#include "cmsis_os.h"

#include "BSP/hw_uart_sic.h"
//#include "BSP/hw_i2c_sys.h"
//#include "BSP/hw_spi_sys.h"
#include "BSP/hw_power_enable.h"

//#include "Drivers/imu_LSM9DS1/imu_LSM9DS1.h"
//#include "Drivers/bme_BME280/bme_BME280.h"
//#include "Drivers/adc_LTC1863/adc_LTC1863.h"
//#include "Drivers/dac_DAC8562/dac_DAC8562.h"
//#include "Drivers/dac_MCP4725/dac_MCP4725.h"
#include "Drivers/periodic_tasks.h"
#include "Drivers/sic_io.h"
#include "Drivers/sic_process_input.h"

// Auto-generated file in {Debug,Release}/
// Commented out until we can find a cross-platform way to do handle this
// Pre-build step:   ${CWD}/../../generate_git_version.sh
//#include "git_version.h"
#define GIT_VERSION "(not defined)"

static void SystemClock_Config(void);

static void ProcessInputThread(void const * argument);
osThreadDef(ProcessInput, ProcessInputThread, osPriorityLow, 0, configMINIMAL_STACK_SIZE * 2);

static void Error_Handler(void);
static void CPU_CACHE_Enable(void);


/// \brief Prints a software banner to the SIC
static void CodeBannerToSic() {
	SicPrintStr( "!! InVADER PDU.   git commit: "GIT_VERSION"   build: "BUILD_TYPE"\r\n" );
	SicPrintf( "!! printf test: %d %d %f\r\n", 8, 0, 3.14159 );
}


static void PeriodicBlinkenlights(const void *data ) {
	powerToggle( NUCLEO_LED_1 );
}


///
/// \brief  Main function for executable
/// \retval Executable return value
///
int main(void)
{
  osThreadId processInputThreadId;

  // STM32F7xx HAL library initialization:
  //	   - Configure the Flash ART accelerator on ITCM interface
  //       - Configure the Systick to generate an interrupt each 1 msec
  //       - Set NVIC Group Priority to 4
  //       - Global MSP (MCU Support Package) initialization
  HAL_Init();

  // Enable the CPU Cache
  CPU_CACHE_Enable();

  // Configure the system clock to 200 MHz
  SystemClock_Config(); 
  

#ifdef DEBUG
  validate();
#endif

  initPowerEnable();

  // UART
  SicBuffersInitialize();
  SicUARTInit();
  CodeBannerToSic();

  // SPI
  //Sys_SPI_Configure();

//  bmeInit();
//  imuInit();
//  dac8562Init();

//  adcInitAll();

  // -- I2C --
  //Sys_I2C_Configure();
//  dacMCPInit( FOCUS_DAC_ADDRESS );

  osTimerDef(periodic, PeriodicBlinkenlights);
  osTimerId periodic_id = osTimerCreate(osTimer(periodic), osTimerPeriodic, (void *)NULL);
  osTimerStart(periodic_id, 1000);

//  Register_Period_Task_ImuSampling( DefaultPeriod_ImuSampling );
//  Register_Period_Task_BmeSampling( DefaultPeriod_BmeSampling );
//  Register_Period_Task_ADCSampling( DefaultPeriod_ADCSampling );
//  Register_Period_Task_I2CDAC( DefaultPeriod_I2CDAC );

  processInputThreadId = osThreadCreate( osThread(ProcessInput), NULL );


  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */
  for( ;; ) {
	  ;
  }
}


static void ProcessInputThread(void const * argument)
{

	while(1) {

		osEvent event = osMessageGet( InputMsgBox, osWaitForever );

		if( event.status == osEventMessage ) {
			uint8_t val = event.value.v;

			sicProcessInput( val );
		}

	//	    osStatus status = osThreadYield();                              //
	//	    if (status != osOK)  {
	//	      // thread switch not occurred, not in a thread function
	//	    }

  }

	osThreadTerminate(NULL);
}


/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 200000000
  *            HCLK(Hz)                       = 200000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 400
  *            PLL_P                          = 2
  *            PLL_Q                          = 9
  *            PLL_R                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  HAL_StatusTypeDef ret = HAL_OK;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is
     clocked below the maximum system frequency, to update the voltage scaling value
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  RCC_OscInitStruct.PLL.PLLR = 7;

  ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
  if(ret != HAL_OK)
  {
    Error_Handler();
  }

  /* Activate the OverDrive */
  ret = HAL_PWREx_EnableOverDrive();
  if(ret != HAL_OK)
  {
    Error_Handler();
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);
  if(ret != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* User may add here some code to deal with this error */
  while(1)
  {
	powerToggle(NUCLEO_LED_3);
	HAL_Delay(1000);
  }
}


/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
  /* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/// @}

