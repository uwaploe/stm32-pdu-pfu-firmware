/// HW Configuration file.  Included in BSP/invader_hw.h
///

#ifndef __INVADER_HW_CONF_H__
#define __INVADER_HW_CONF_H__

/// \addtogroup PDU_firmware
/// @{

//======= Enable VCP Serial port (optional) =======
#define INVADER_VCP_AS_SIC 1

//======= Enable use of idiot LEDs on Nucleo board ======
#define INVADER_USE_NUCLEO_LEDS 1

/// @}


#endif
