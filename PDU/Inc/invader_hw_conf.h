/// HW Configuration file.  Included in BSP/invader_hw.h
///

#ifndef __INVADER_HW_CONF_H__
#define __INVADER_HW_CONF_H__

/// \addtogroup PDU_firmware
/// @{

//======= Declare that this project targets the PDU hardware =======
#define INVADER_BOARD_PDU

/// @}


#endif
