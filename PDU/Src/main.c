///
///
/// Main file for InVADER PDU firmware.
///

/// \addtogroup PDU_firmware
/// @{

/* Includes ------------------------------------------------------------------*/
#include "BSP/invader_hw.h"

#ifdef DEBUG
#include "BSP/invader_hw_validate.h"
#endif

#include "cmsis_os.h"

#include "BSP/hw_uart_sic.h"
#include "BSP/hw_i2c_sys.h"
#include "BSP/hw_spi_sys.h"
#include "BSP/hw_power_enable.h"
#include "BSP/dwt_delay.h"

#include "Drivers/global_state.h"
#include "Drivers/focus_motor/focus_motor.h"

#include "Drivers/imu_LSM9DS1/imu_LSM9DS1.h"
#include "Drivers/imu_LSM9DS1/imu_periodic_task.h"

#include "Drivers/bme_BME280/bme_BME280.h"
#include "Drivers/bme_BME280/bme_periodic_task.h"

#include "Drivers/adc_LTC1867/adc_LTC1867.h"
#include "Drivers/adc_LTC1867/adc_periodic_task.h"

#include "Drivers/dac_MCP4725/dac_MCP4725.h"

#include "Drivers/hcb_uart/hcb_io.h"
#include "Drivers/hcb_uart/hcb_uart.h"

#include "Drivers/periodic_tasks.h"

#include "Drivers/sic_io.h"

// Auto-generated file in {Debug,Release}/
//!! Commented out until we can find a cross-platform way to do handle this
//!! Pre-build step:   ${CWD}/../../generate_git_version.sh
//#include "git_version.h"
#define GIT_VERSION "(not defined)"

/// \brief Prints a software banner to the SIC
static void CodeBannerToSic() {
	SicPrintStr( "!! == InVADER PDU.   git commit: "GIT_VERSION"   build: "BUILD_TYPE" ==\r\n" );
	//SicPrintf( "!! printf test: %d %d %f\r\n", 8, 0, 3.14159 );
}



///
/// \brief  Main function for executable
/// \retval Executable return value
///
int main(void)
{

  // STM32F7xx HAL library initialization:
  //	   - Configure the Flash ART accelerator on ITCM interface
  //       - Configure the Systick to generate an interrupt each 1 msec
  //       - Set NVIC Group Priority to 4
  //       - Global MSP (MCU Support Package) initialization
  HAL_Init();

  // Enable the CPU Cache
  CPU_CACHE_Enable();
  PDU_SystemClock_Config();
  
  // Initialize the global state variables (this also initializes the Backup domain/RTC)
  initGlobalState();

  // Initialize DWT timer for microsecond delays
  DWT_Init();

  // Initialize UART communications
  SicBuffersInitialize();
  SicUARTInit();
  CodeBannerToSic();



#ifdef DEBUG
  // Validation checks the internal data tables.
  // Should only be run on test version of the firmware
  if( !validate() ) {
	  SicPrintStr("!!! == VALIDATION FAILED ==\r\n");
  }
#endif

  // -- GPIO power enables --
  initPowerEnable();

  // -- GPIO for the focus motor IRQ-inputs and control outputs --
  initFocusMotor();

  // -- UART to the HCB --
  HcbUartInit();

  // -- Initialize SPI devices --
  Sys_SPI_Configure();
  bmeInit();
  imuInit();

  adcInitAll();

  // -- Initialize I2C devices --
  Sys_I2C_Configure();
  dacMCPInit( FOCUS_DAC_ADDRESS );

  Register_PeriodicTask_GlobalState( DefaultPeriod_GlobalState );
  Register_PeriodicTask_FocusStatus( DefaultPeriod_FocusStatus );

  Register_PeriodicTask_ImuSampling( DefaultPeriod_ImuSampling );
  Register_PeriodicTask_BmeSampling( DefaultPeriod_BmeSampling );
  Register_PeriodicTask_SystemADCs( DefaultPeriod_ADCSampling );

  osThreadCreate( osThread( ProcessSicInput ), NULL );
  osThreadCreate( osThread( ProcessHcbInput ), NULL );

  osKernelStart();
  
  /* We should never get here; control is now taken by the scheduler */
  for( ;; ) {
	  ;
  }
}




/// @}

