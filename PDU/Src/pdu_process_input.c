///
/// @file  pdu_process_input.h
/// #brief Processes incoming SIC commands to PDU
///

#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "Drivers/sic_io.h"
#include "Drivers/hcb_uart/hcb_io.h"

#include "BSP/hw_power_enable.h"

#include "Drivers/laser_control/laser_control.h"
#include "Drivers/focus_motor/focus_motor.h"
#include "Drivers/dac_MCP4725/dac_MCP4725.h"
#include "Drivers/dac_DAC8562/dac_DAC8562.h"

/// \addtogroup Common @{
/// \addtogroup Drivers @{
/// \addtogroup SicIO SIC Input/Output @{


static void sicProcessLaserMode( char *strtok_state )
{
	SicBufferId_t outBuf = SicBufferReserve();

	SicBufferAppendStr( outBuf, "$LASER" );

	char *status;

	status = strtok_r( NULL, ",*", &strtok_state );
	if( status == NULL ) {
		SicBufferAppendStr( outBuf, ",ERR");
		goto done;
	}

	// \TODO.  More sophisticated/robust parser
	int value = atoi( status );

	if( value == 1 ) {
		enableLaserMode();
	} else if( value == 0 ) {
		disableLaserMode();
	} else {
		SicBufferAppendStr( outBuf, ",ERR");
		goto done;
	}

	// \TODO.   Function to convert status code to string?
	SicBufferAppendPrintf( outBuf, ",%d", LaserStatus.status );

done:

	SicBufferFinalizeNMEA( outBuf );

}



static void sicProcessLaserPower( char *strtok_state )
{
	if( !isLaserMode() ) {
		SicNMEAPrintf("LPOWER","OFF");
		return;
	}

	SicBufferId_t outBuf = SicBufferReserve();

	SicBufferAppendStr( outBuf, "$LPOWER" );

	char *circuit, *status;
	while( (circuit = strtok_r( NULL, ",*", &strtok_state)) ) {

		// \TODO.  More sophisticated/robust parser
		int whichVoltage = atoi( circuit );

		enum PowerEnable whichCircuit = POWER_ENABLE_NOT_FOUND;
		if( whichVoltage == 12 ) {
			whichCircuit = LASER_ISO_12;
		} else if( whichVoltage == 28 ) {
			whichCircuit = LASER_ISO_28;
		} else {
			SicBufferAppendStr( outBuf, ",ERR");
			goto done;
		}

		SicBufferAppendPrintf( outBuf, ",%d", whichVoltage );

		status = strtok_r( NULL, ",*", &strtok_state );
		if( status == NULL ) {
			SicBufferAppendStr( outBuf, ",ERR");
			goto done;
		}

		// \TODO.  More sophisticated parser
		int value = atoi( status );

		if( value == 1 ) {
			powerOn( whichCircuit );

			//// \todo This could be done in a more sophisticated way...
			if( whichCircuit == LASER_ISO_12 ) {
				HAL_Delay(100);
			   dac8562Init();			/// Initialization sets the DAC to 0
			}

		} else if( value == 0 ) {
			powerOff( whichCircuit );
		} else {
			SicBufferAppendStr( outBuf, ",ERR");
			goto done;
		}

		SicBufferAppendPrintf( outBuf, ",%d", value );
	}

done:

	SicBufferFinalizeNMEA( outBuf );


}


static void sicProcessLaserDac( char *strtok_state )
{
	// \todo TBD to be set.
	const uint16_t LaserDacMax = 0x7FFF;

	if( !isLaserMode() ) {
		SicNMEAPrintf("LDAC","OFF");
		return;
	}


	SicBufferId_t outBuf = SicBufferReserve();

	SicBufferAppendStr( outBuf, "$LDAC," );

	char *valStr;

	valStr = strtok_r( NULL, ",*", &strtok_state );
	if( valStr == NULL ) {
		SicBufferAppendStr( outBuf, ",ERR");
		goto done;
	}

	// \TODO.  More sophisticated parser
	uint16_t value = (int)strtol( valStr, NULL, 16 );

	if( value > LaserDacMax ) value = LaserDacMax;

	//\TODO Reset DAC every time.  Ugly!
	//dac8562Init();
	dac8562Set( value );

	// \TODO.   Function to convert status code to string?
	SicBufferAppendPrintf( outBuf, ",%04X", value );

done:

	SicBufferFinalizeNMEA( outBuf );

}

static void sicProcessFocusDac( char *strtok_state )
{

	if( !powerState(FOCUS_EN) ) {
		SicNMEAPrintf("FDAC","OFF");
		goto done;
	}


	SicBufferId_t outBuf = SicBufferReserve();

	SicBufferAppendStr( outBuf, "$FDAC" );

	char *valStr;

	valStr = strtok_r( NULL, ",*", &strtok_state );
	if( valStr == NULL ) {
		SicBufferAppendStr( outBuf, ",ERR");
		goto done;
	}

	// \TODO.  More sophisticated parser
	uint16_t value = (int)strtol( valStr, NULL, 16 );

	if( !dacMCPWrite( FOCUS_DAC_ADDRESS, value ) ) {
		SicBufferAppendStr( outBuf, ",ERR");
		goto done;
	}

	// \TODO.   Function to convert status code to string?
	SicBufferAppendPrintf( outBuf, ",%04X", value );

done:

	SicBufferFinalizeNMEA( outBuf );

}


static void sicProcessFocus( char *strtok_state )
{
	if( !powerState(FOCUS_EN) ) {
		SicNMEAPrintf("FOCUS","OFF");
		goto done;
	}

	SicBufferId_t outBuf = SicBufferReserve();

	SicBufferAppendStr( outBuf, "$FOCUS" );

	char *valStr;
	valStr = strtok_r( NULL, ",*", &strtok_state );
	if( valStr == NULL ) {
		SicBufferAppendStr( outBuf, ",ERR");
		goto done;
	}

	if( !strncmp( valStr, "ZERO", 4 ) ) {

		focusMotorZeroEncoder();
		SicBufferAppendPrintf( outBuf, ",ZERO" );

	} else if( !strncmp( valStr, "STOP", 4 ) ) {

			focusMotorStop( false );
			SicBufferAppendPrintf( outBuf, ",STOP" );

	} else if( !strncmp( valStr, "SEEK", 4 ) ) {

		SicBufferAppendStr( outBuf, ",SEEK");

		int32_t value;

		valStr = strtok_r( NULL, ",*", &strtok_state );
		if( valStr != NULL ) {
			value = atoi( valStr );
		}

		focusMotorSeek( value );

		SicBufferAppendPrintf( outBuf, ",%d", value );

	} else if ( !strncmp( valStr, "JOG", 3 ) ) {

		SicBufferAppendStr( outBuf, ",JOG");

		valStr = strtok_r( NULL, ",*", &strtok_state );
		if( valStr == NULL ) {
			SicBufferAppendStr( outBuf, ",ERR");
			goto done;
		}

		bool doForward;
		if( valStr[0] == 'F' ) {
			doForward = true;
		} else if (valStr[0] == 'R') {
			doForward = false;
		} else {
			SicBufferAppendStr( outBuf, ",ERR");
			goto done;
		}

		/// Use 1000ms as default value
		uint16_t value = 1000;

		valStr = strtok_r( NULL, ",*", &strtok_state );
		if( valStr != NULL ) {
			value = atoi( valStr );
		}

		focusMotorJog( doForward, value );		// Delay in milliseconds

		SicBufferAppendPrintf( outBuf, ",%c,%d", doForward ? 'F' : 'R', value );

	} else {
		SicBufferAppendStr( outBuf, ",ERR");
		goto done;
	}


done:

	SicBufferFinalizeNMEA( outBuf );

}



static void sicProcessHCB( char *strtok_state )
{
	SicBufferId_t outBuf = SicBufferReserve();

	SicBufferAppendStr( outBuf, "$HCB," );

	// Just pass command through the HCB


	char *command;
	if( (command = strtok_r( NULL, "*", &strtok_state)) )
	{
		HcbPrintf("$%s\r\n", command);
		SicBufferAppend( outBuf, command, strlen(command) );
	} else {
		SicBufferAppendStr( outBuf, ",ERR");
	}



//		if( strcmp( command, "FAN" ) == 0 )
//		{
//			value = strtok_r( NULL, ",*", &strtok_state );
//			if( value == NULL )
//			{
//				SicBufferAppendStr( outBuf, ",ERR");
//				goto done;
//			}
//
//			if( strcmp( command, "OFF" ) == 0 )
//			{
//				HCBBufferAppendStr("$FAN,OFF");
//
//				//send to HCB $FAN
//			}
//			else if( strcmp( command, "LOW" ) == 0 )
//			{
//				SicBufferAppendStr( outBuf, ",LOW");
//				HCBBufferAppendStr("$FAN,LOW");
//				//send to HCB $FAN
//			}
//			else if( strcmp( command, "MED" ) == 0 )
//			{
//				SicBufferAppendStr( outBuf, ",MED");
//				HCBBufferAppendStr("$FAN,MED");
//				//send to HCB $FAN
//			}
//			else if( strcmp( command, "HIGH" ) == 0 )
//			{
//				HCBBufferAppendStr("$FAN,HIGH");
//				SicBufferAppendStr( outBuf, ",HIGH");
//
//				//send to HCB $FAN
//			}
//			else
//			{
//				SicBufferAppendStr( outBuf, ",ERR Bad Fan Choice");
//				goto done;
//			}
//			//fan stuff
//		}
//		if( strcmp( command, "ADC" ) == 0 )
//		{
//			HCBBufferAppendStr("$ADC");
//		}

//	}


	done:
		SicBufferFinalizeNMEA( outBuf );
}



///==== Main entry ====

void processSicInput( SicRxBuffer_t *msg )
{

	// Convert message to upper
	for( int j = 0; j < msg->length; ++j ) {
		msg->buf[j] = toupper(msg->buf[j]);

		// Cut off any trailing line endings
		if( (msg->buf[j] == '\r') || (msg->buf[j] == '\n') ) msg->buf[j] = '\0';
	}

	// Look for message delimiter
	msg->parserCursor = msg->buf;
	for( ; (*msg->parserCursor != '$') && (*msg->parserCursor != '\0'); ++msg->parserCursor ) {;}

	if( *msg->parserCursor == '\0' ) return;

	// Skip past delimiter
	msg->parserCursor++;
	char *rest = msg->parserCursor;
	char *nmeaMsgType = strtok_r( msg->parserCursor, ",*", &rest );

	if( nmeaMsgType == NULL ) return;

	// Break out based on message type.
	if( !strncmp( nmeaMsgType, "PWREN", 5 ) || !strncmp( nmeaMsgType, "POWER", 5 ) ) {
		sicProcessPowerEnable( rest );
	} else if( !strncmp( nmeaMsgType, "LASER", 5 ) ) {
		sicProcessLaserMode( rest );
	} else if( !strncmp( nmeaMsgType, "LPOWER", 6 ) ) {
		sicProcessLaserPower( rest );
	} else if( !strncmp( nmeaMsgType, "LDAC", 4 ) ) {
		sicProcessLaserDac( rest );
	} else if( !strncmp( nmeaMsgType, "FDAC", 4 ) ) {
		sicProcessFocusDac( rest );
	} else if( !strncmp( nmeaMsgType, "FOCUS", 5 ) ) {
		sicProcessFocus( rest );
	} else if( !strncmp( nmeaMsgType, "HCB", 3 ) ) {
		sicProcessHCB( rest );
	}
}



/// @} @} @}



