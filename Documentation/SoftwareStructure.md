# Software Structure

This repository contains multiple standalone  [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) / Eclipse projects:

 * `PDU/` contains the "flight" firmware for the Power Distribution board in the A2 bottle.
 * `PFU/` contains the "flight" firmware for the Power Filter board in the A1 bottle.

Whenever possible, code is shared between all of the projects.   This is
stored in the `common/` directory.  The projects each import the required
code from `common/` using virtual links in Eclipse (like symlinks).

Within `common/`:

 * `BSP` is the common board support package.  It contains functions which interface
  with chip hardware through STM HAL and low-level (LL) functions.  

  These functions are configured (pin mappings, etc.) via the `BSP/invader_hw.h` header.  This file in turn pulls board-specific configuration data from a file `invader_hw_conf.h` which is provided by each project.

  This directory contains the BSP which is majority shared between boards.   Very board/project-specific BSP is included in the specific projects.

 * `Drivers` contains shared application code.  Originally this was drivers for the various external (SPI, etc) hardware, but this directory now also contains other algorithmic code chunks.   These functions rely on functions provided by the BS in an effort to provide a degree of abstraction.

 So, for example:

  * In `Drivers/adc/adc.c`, `adcQueryADC()` calls `adcReadFromSPI()`
  * `BSP/invader_hw.h` provides `adcReadFromSPI()` as a thin wrapper around `HAL_SPI_ReadData()`.  It uses configuration information (which SPI bus, which chip select) provided in `invader_hw.h`

 Note these code chunks are _not_ compiled into a library.  Instead, different projects reflect their hardware differences by including/not including different source files from `common/`

`common` also includes the following "stock" code from STMCube.

 * `CMSIS` is the STM-provided [Cortex Microcontroller Software Interface Standard](https://www.arm.com/why-arm/technologies/cmsis), a hardware abstraction layer shared between ARM vendors.

 * `STM32F7xx_HAL_Driver` is the STM-provided, STM32-specific Hardware abstraction layer.  It contains both high level and low-level `ll` functions for interfacing with the STM chip hardware.

 * `Startup` is STM-provided chip bootstrap code (in assembly)
