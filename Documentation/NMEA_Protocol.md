# PDU / PFU NMEA Protocol

The primary communications protocol to both the PDU and PFU is based (roughly)
on the standard NMEA 0183 protocol.   For the PDU, this protocol is
implemented over the RS-232  link to the SIC, for the PFU is is
_(something something ethernet)_.

The protocol contains three classes of messages:

 * __Status messages__ contain regularly or periodically scheduled data from the PDU/PFU, and are send asynchronously from the PDU/PFU to the SIC.

 * __Command messages__ are sent from the SIC to the PDU/PFU.

 * __Response messages__ are sent from the PDU/PFU in response to command
 messages.

## Protocol details

Every message will start with a ‘$’ delimiter, and will end with <CR>-<LF>
A single message is limited to 256 characters, including the delimiters and line endings.   Any additional characters will be ignored.

Every message will have the format:

    $ABCDE,a,b,c,d,e,f*XX<CR><LF>

Where ABCDE is a 5-character tag which identifies both the message source (always the PDU) and message type.  The PDU will always produce all-capital identifiers.  However, it will parse command messages in a case-insensitive manner e.g ‘abcde’ ‘AbCdE’ and ‘ABCDE’ are all considered the same identifier.

A command will contain a number of fields, delimited by commas, with the precise number, ordering and meaning of the fields determined by the message type.   Fields are identified by field order.  Empty (zero-length) fields are allowed.

The command ends with an asterisk followed by a two-character checksum.  The
checksum is the two-byte hexadecimal representation of the XOR of all
characters between the $ and \*, exclusive.   To allow interactive use, the
checksum is optional for command messages. If not used, the asterisk and
checksum characters are omitted.   If provided, the checksum must be correct
or the command will be ignored.  

While booting the board may send a number of diagnostic messages.   These
lines will start with two exclamation points ‘!!’ and end with a CR-LF.   
Once the NMEA-style output is enabled, no further non-NMEA diagnostic
messages will be sent.

## PDU connectivity

The PDU communicates with the SIC over an RS232 serial port at 115200 baud, 8N1.


-----

# Sensors

## Subsystem:  LSM9DS1 IMU

Both the PDU and PFU have a [LSM9DS1](https://www.st.com/en/mems-and-sensors/lsm9ds1.html) MEMs gyro/magnetometer.

### Status messages

### Command and response messages
